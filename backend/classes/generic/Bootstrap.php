<?php
/*
	use URL to determine which controller to instantiate
	
	domain/[controller]/[method]/[parameter1] => index.php?url=/[controller]/[method]/[parameter1]
*/
class Bootstrap{

	public function __construct(){
		session_start();
		$process = $_SERVER['REQUEST_URI'];
		if( $process != '/'){
			$uriArray = preg_split('/\//', $process, -1, PREG_SPLIT_NO_EMPTY);
			
			if(DEBUG){
				$output = "<p>".$process."</p>\n";
				foreach($uriArray as $key => $value){
					$output .= "<p>$key => $value</p>\n";
				}
				echo $output;
			}
					
			if( $uriArray[0] != '' && class_exists($uriArray[0]) ){ // check to see if controller exists
				$app  = new $uriArray[0]; // instantiate controller
				
				if( isset($uriArray[1]) && method_exists($app,$uriArray[1])){ // check to see if controller->method exists
					if( isset($uriArray[2]) ){ // is there a parameter?
						if( isset($uriArray[3]) ){ // are there two parameter?
							if( isset($uriArray[4]) ){ // are there two parameter?
								$app->{$uriArray[1]}($uriArray[2], $uriArray[3], $uriArray[4]); // run method with parameters
							}else{
								$app->{$uriArray[1]}($uriArray[2], $uriArray[3]); // run method with parameter
							}
						}else{
							$app->{$uriArray[1]}($uriArray[2]); // run method with parameter
						}
					}else{
						$app->{$uriArray[1]}(); // run method without parameter
					}

				}else{
					$app->defaultPage($uriArray);
				}
			}else{
				echo "page error ".$uriArray[0];
				$app = new root();
				// check to see if root template exists, if not display error page
				$app->error();
			}
		}else{
			$app = new root();
			$app->home();
		}
	}
}