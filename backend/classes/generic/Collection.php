<?php
/*
addItem($items) // add single object
removeItems($indices) // remove items specified in array of indices
updateItems($index,$updatedItem) // update item in collection, replace with $updatedItem
sortItems($property,$order = 'ASC') // not implemented yet
firstItem() // point cursor to first item
lastItem() // point cursor to last item
indexItem($index) // set cursor to specified index
iterate($callback,$wrapper,$direction = TRUE,$parameters) //  iterate through items
currentItem() // return item pointed to by cursor
itemCount() // return number of items
cursor() // return value of cursor
*/
class Collection{

	protected $items = array();
	protected $itemCount;
	protected $cursor;
	protected $tempCursor;
	
	function __construct(){
		$this->emptyCollection();
	}
	
	function __destruct(){
	}
	
	function emptyCollection(){
		$this->items = array();
		$this->itemCount = 0;
		$this->cursor = 0;
		$this->tempCursor = 0;
	}
	
	function addItem( $items ){
		if( is_array( $items ) ){
		}else if( is_object( $items ) ){
			array_push( $this->items, $items );
		}
		$this->itemCount = count( $this->items );
	}
	
	function removeItems( $indices ){
		if( is_array( $indices ) ){
			for($n = count($indices); $n > -1; $n--){
				array_splice($this->items,$indices[$n] , 1);
			}
		}else{
			array_splice($this->items,$indices , 1);
		}
		$this->itemCount = count($this->items);
	}
	
	function updateItems($index,$updatedItem){
		if($this->indexItem($index)){
			$this->items[$index]->__setMultiple($updatedItem); // references framework->__setMultiple() which accepts an associative array of property key/value pairs
			return true;
		}else
			return false;
	}
	
	function sortItems($property,$order = 'ASC'){ // $property - which property of item to use as sort criterion = ASC|DESC
	
		switch($order){
			case 'ASC' : {
			} break;
			case 'DESC' : {
			} break;
		}
	}
	
	function firstItem(){
		$this->cursor = 0;
	}
	
	
	function lastItem(){
		$this->cursor = $this->itemCount - 1;
	}
	
	function indexItem($index){
		if( ($index > 0) && ($index < $this->itemCount) ){
			$this->cursor = $index;
			return true;
		}else{
			return false;
		}
	}
	
	function nextItem(){
		if($this->cursor < ($this->itemCount - 1) ){
			$this->cursor++;
			return true;
		}else{
			return false;
		}
	}
	
	function previousItem(){
		if($this->cursor > 0){
			$this->cursor--;
			return true;
		}else{
			return false;
		}
	}
	
	/*
	http://www.electricmonk.nl/log/2007/09/26/callback-functions-in-php/
	
	callback = the NAME of the method to apply for each item
	ascending order = TRUE (DEFAULT), descending order = FALSE
	
	call_user_func_array( $callBackDefine,array($parameters) )
	
	$callBackDefine = array(
		$this->currentItem(),	// owner of method
		$callback				// method name
	)
	
	array(parameters) ->
	
	array of values, number of values = number of parameters in function definition
	
	the method showBlogs(a,b,c) has three arguments so the array passed to the method will need three elements
	You can pass array variables as arguments by wrapping the variable in an array declaration as below
	*/
	function iterate( $callback, $parameters = NULL, $direction = TRUE){
		$this->tempCursor = $this->cursor;
		$output = '';
		switch( $direction ){
			case TRUE : {
				$this->firstItem();
				do{
					$parameters['index'] = $this->cursor;
					$callBackDefine = array($this->currentItem(),$callback);
					$output .=  call_user_func_array( $callBackDefine, array($parameters) );
				}while( $this->nextItem() );
			} break;
			case FALSE : {
				$this->lastItem();
				do{
					$parameters['index'] = $this->cursor;
					$callBackDefine = array($this->currentItem(),$callback);
					$output .= call_user_func_array( $callBackDefine,array($parameters) );
				}while( $this->previousItem() );
			} break;
		}
		$this->cursor = $this->tempCursor;
		return $output;
	}
	
	function currentItem(){
		return $this->items[$this->cursor];
	}
	
	function itemCount(){
		return $this->itemCount;
	}
	
	function cursor(){
		return $this->cursor;
	}
}