<?php
class XSLTransform
{
	// properties
	var $xml;
	var $xsl;
	var $work;

	function XSLTransform($xmlPath = null,$xslPath = null,$paramaterArray = null){
		if(!is_null($xmlPath))
			$this->setXML($xmlPath);
		if(!is_null($xslPath ))
			$this->setXSL($xslPath );
		if(!is_null($paramaterArray))
			$this->setXSLParameters($paramaterArray);
	}

	function setXML($xmlPath){
		$this->xml = new DOMDocument();
		$this->xml->load($xmlPath);
	}

	function setXSL($xslPath){
		$doc = new DOMDocument();
		$doc->load($xslPath);
		$this->xsl = new XSLTProcessor();
		$this->xsl->importStyleSheet($doc);
	}
		
	function setXSLParameters($paramaterArray){
		foreach($paramaterArray as $parameter){
			$this->xsl->setParameter('', $parameter["name"], $parameter["value"]);
			// echo "<p>".$parameter["name"]." = ".$parameter["value"]."</p>\n";
		}
	}

	function transformToXML($omitXMLdoctype){
		$this->work = new DOMDocument();
		$this->work->loadXML($this->xsl->transformToXML($this->xml));

		if($omitXMLdoctype){
			return $this->work->saveXML($this->work->documentElement);
		}else{
			return $this->work->saveXML();
		}
	}
}

?>
