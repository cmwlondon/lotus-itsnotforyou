<?php
class view{
	public function __construct($controllerName = null){
		$this->controllerName = $controllerName;
	}
	
	public function setParameters($viewParameters){
		$this->parameters = $viewParameters;
	}

	public function getParameter($parameterName){
		if( property_exists($this->parameters,$parameterName) ){
			return $this->parameters->$parameterName;
		}else{
			return false;
		}
	}
	
	// render complete web page
	public function renderPage($name){
		$this->parameters->thisPage = $_SERVER['REQUEST_URI'];
		$this->parameters->controller = $this->controllerName;
		$this->parameters->method = $name;
	
		$this->parameters->userDropdown = "";
		$users = new User_Collection();
		if( $users->load() ){
			foreach($users->load() as $user){
				$this->parameters->userDropdown .= "<li><a href=\"/users/details/".$user["id"]."/".$user["username"]."\">".$user["username"]."</a></li>\n";
			}
		}

		$this->parameters->groupDropdown = "";
		$groups = new Group_Collection();
		if( $groups->load() ){
			foreach($groups->load() as $group){
				$this->parameters->groupDropdown .= "<li><a href=\"/groups/details/".$group["id"]."/".$group["title"]."\">".$group["title"]."</a></li>\n";
			}
		}

		$this->parameters->permissionDropdown = "";

		$this->parameters->serviceDropdown = "";
		/*
		$services = new Service_Collection();
		if( $services->load() ){
			foreach($services->load() as $service){
				$this->parameters->serviceDropdown .= "<li><a href=\"/service/details/".$service["id"]."/".$service["title"]."\">".$service["title"]."</a></li>\n";
			}
		}
		*/#

		$this->parameters->categoryDropdown = "";

		$this->parameters->imageDropdown = "";
		$defaultImages = new Image_Collection();
		$this->parameters->imageCount = $defaultImages->imageCount();
		$this->parameters->imageDropdown = "<li><a href=\"/images/category/0\">All (".$this->getParameter('imageCount')." images)</a></li>\n";
		foreach( $defaultImages->getCategories() as $catFilter){
			$this->parameters->imageDropdown .= "<li><a href=\"/images/category/".$catFilter["categoryID"]."\">".$catFilter["categoryName"]." (".$catFilter["imageCount"]." images)</a></li>\n";
		}
				
		$this->parameters->articleDropdown = "";
		$latestArticles = new Article_Collection();
		foreach( $latestArticles->latestPublished(10) as $article){
			$this->parameters->articleDropdown .= "<li><a href=\"/articles/details/". $article["id"]."\">". $article["title"] ."</a></li>\n";
		}

		$this->parameters->documentDropdown = "";
		$latestDocuments = new Document_Collection();
		foreach( $latestDocuments->latest(10) as $document){
			$this->parameters->documentDropdown .= "<li><a href=\"/documents/details/". $document["id"]."\">". $document["title"] ."</a></li>\n";
		}

		// build breadcrumb trail based on URL : controller/method/[ID=>title]
		require VIEWS."shared/header.php";
		require VIEWS."shared/breadcrumb.php";
		require VIEWS.$this->controllerName."/".$name."/template.php";
		require VIEWS."shared/footer.php";
	}
	
	// render html content, inserted into existing page via template or AJAX call
	public function renderHTML($name){
		require VIEWS.$this->controllerName."/".$name."/html.php";
	}
	
	// render JSON object
	public function renderJSON($name){
		require VIEWS.$this->controllerName."/".$name."/json.php";
	}
	
	// render XML file
	public function renderXML($name){
		require VIEWS.$this->controllerName."/".$name."/xml.php";
	}
	
	// render CSV file
	public function renderCSV($name){
		require VIEWS.$this->controllerName."/".$name."/template.php";
	}
	
	// download document or image file
	public function download($name){
		require VIEWS.$this->controllerName."/".$name."/template.php";
	}
}
?>