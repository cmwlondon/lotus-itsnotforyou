<?php
// MySQL PDO version
class Database{

	public $connection;
	public $status;
	
	public function __construct(){
	}

	public function connect($connectionData){
		try{
			$this->connection = new PDO("mysql:host=".$connectionData["host"].";dbname=".$connectionData["database"], $connectionData["user"], $connectionData["password"] );
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->connection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
			$this->status = true;
			
		}
		catch(PDOException $e){
			$this->status = false;
			echo $e->getMessage();
		}
	}
	
	public function disconnect(){
		$this->connection = null;
	}
}
?>