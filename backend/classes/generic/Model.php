<?php
class Model{

	protected $properties;
	
	public function __construct(){
		$this->properties = array();
		debug( "Model constructor", 0 );

	}
	
	public function __set( $propertyName, $propertyValue ){
		debug( "Model->__set $propertyName - $propertyValue", 0 );
		$this->properties[ $propertyName ] = $propertyValue;
	}
	
	public function __get( $propertyKey ){
		debug( "Model->__get $propertyKey", 0 );
		if( array_key_exists( $propertyKey, $this->properties ) ){
			return $this->properties[ $propertyKey ];
		}else{
			return NULL;
		}
	}

	function multiSet( $propertyNames ){
		debug( "Model->multiSet", 0 );

		foreach( $propertyNames as $propertyName => $propertyValue ){
			$this->__set( $propertyName, $propertyValue );
		}
	}
	
	function multiGet( $propertyNames ){
		debug( "Model->multiGet", 0 );

		$foundProperties = array();
		if( is_array( $propertyNames ) ){
			foreach( $propertyNames as $propertyKey ){
				$foundProperties[ $propertyKey ] = $this->__get( $propertyKey );
			}
			return $foundProperties;
		}else{
			return $this->__get( $propertyNames );
		}
	}

	
	public function getFieldType($field){
	}
	
	public function build( $propertyArray ){
		debug( "Model->build", 0 );

		foreach($propertyArray as $field => $value){
			$this->__set($field, $value);
		}
	}

	public function objectToArray(){
		debug( "Model->objectToArray", 0 );
		return get_object_vars( $this->properties );
	}
	
	public function showAll(){
		debug( "Model->showAll", 0 );

		echo "<ul>\n";
		foreach($this->properties as $field => $value){
			echo "<li>$field : [$value]</li>\n";
		}
		echo "</ul>\n";
	}
}
?>