<?php
/*
generic object framework
includes method for setting/getting object properties

__set / __setSingle ($propertyName,$propertyValue)
__setMultiple ($parameterArray)
__getSingle ($propertyName)
__showProperties()
*/

class Framework{
	private $objectData;

	function __construct($parameters = null){
		$this->objectData = array();
		
		if( !is_null($parameters) ){
			$this->__setMultiple($parameters);
		}
	}
	
	function __set($propertyName,$propertyValue = null){
		$this->objectData[$propertyName] = $propertyValue;
	}

	function __setSingle($propertyName,$propertyValue){
		$this->objectData[$propertyName] = $propertyValue;
	}

	/*
	pass properties in as associative array
	$parameterArray = [
		'a' => 'aa',
		'b' => 'b'
	]
	*/
	function __setMultiple($parameterArray){
		foreach($parameterArray as $key => $value){
			$this->__set($key,$value);
		}
	}

	/*
	return specified properties
	single property, pass in property name as string, returns property value
	multiple properties pass in array of property names, returns associative array
	*/
	function __get($propertyNames){
		$parameterType = gettype($propertyNames);
		
		switch($parameterType){
			case "string" : {
				return $this->__getSingle($propertyNames);
			} break;
			case "array" : {
				$returnArray = array();
				foreach($propertyNames as $propertyName){
					$returned = $this->__getSingle($propertyNames);
					if($returned)
						$returnArray[$propertyName] = $returned;
				}
				return $returnArray;
			} break;
			case "object" : {
			} break;
		}
	}

	// returns value of property if property is defined, otherwise it returns FALSE
	function __getSingle($propertyName){
		// add check to see if property exists
		$returnValue = ( array_key_exists($propertyName,$this->objectData) ) ? $this->objectData[$propertyName] : false ;
		return $returnValue;
	}
	
	function __showProperties(){
		$output = "<ul>\n";
		foreach($this->objectData as $key => $value){
			$output .= "<li>".$key." =&gt; ".$value."</li>\n";
		}
		$output .= "</ul>\n";
		return $output;
	}
}
?>