<?php
class Controller{
	
	public $registry;
	public $view;
	
	public function __construct($registry, $controllerName = null){
		if(DEBUG)
			echo "<p>Controller Name: $controllerName</p>\n";

		$this->registry = $registry;
		
		if(isset($controllerName)){
			$this->view = new View( $controllerName );
			$this->viewParameters = new stdClass();
		}
	}
	
	// call arbitrary method of this controller object [*method*]
	public function contentModule( $matches ){
		return $this->{ $matches[1] }();
	}
	
	public function getTemplate( $templateFile ){
		$handle = fopen($templateFile, "r");
		$template = fread($handle, filesize($templateFile));
		fclose($handle);
		return $template;
	}
	
	public function error(){
		$this->viewParameters->title = '404';
		$this->view->setParameters($this->viewParameters);
		$this->view->renderPage('error');
	}

	public function isUserRegistered(){
		if(DEBUG)
			echo "<p>controller->isUserRegistered</p>\n";
			
		if ( isset($_SESSION['userid'])){
			if(DEBUG)
				echo "<p>Registered user id: ".$_SESSION['userid']."</p>\n";
				
			if ( $_SESSION['userid'] != -1){
				// user is logged in
				
				$this->showLogin = false;
				$this->registeredUser = new User_Model( $this->registry );
				$this->registeredUser->load( $_SESSION['userid'] );
				$this->registeredUserName = $this->registeredUser->__get("username");
				$this->registeredID = $this->registeredUser->__get("id");
				return true;
			}else{
				return $this->handleLogin();
			}
		}else{
			return $this->handleLogin();
		}
	}
	
	public function handleLogin(){
		if(DEBUG)
			echo "<p>controller->handleLogin</p>\n";
			
		// is the user in the proces of logging in?
		if( isset($_POST["user"]) && isset($_POST["password"]) ){
			// yes, collect username/password
			$user = new User_Model( $this->registry );
			if( $user->doLogin( $_POST["user"], $_POST["password"] )){
				$this->showLogin = false;
				$this->registeredUser = $user;
				$this->registeredUserName = $this->registeredUser->__get("username");
				$this->registeredID = $this->registeredUser->__get("id");
				$_SESSION['userid'] = $this->registeredID;
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}