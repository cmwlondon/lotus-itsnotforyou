<?php
class Register_PDO extends Database{


	private $getLatestIDPS;
	private $setLatestIDPS;
	
	public function __construct(){
		parent::__construct();
	
		global $dbConnections;
		$this->connect(array(
			"host" => $dbConnections[DEFAULT_DB]["host"],
			"database" => $dbConnections[DEFAULT_DB]["db"],
			"user" => $dbConnections[DEFAULT_DB]["user"],
			"password" => $dbConnections[DEFAULT_DB]["password"]
		));

		$this->selectAllPS = $this->connection->prepare("SELECT * FROM register ORDER BY id ASC");
		$this->selectAllPS->setFetchMode(PDO::FETCH_ASSOC);

		$this->selectByIDPS = $this->connection->prepare("SELECT * FROM register WHERE id = :id");
		$this->selectByIDPS->bindParam(':id', $this->id, PDO::PARAM_INT);
		$this->selectByIDPS->setFetchMode(PDO::FETCH_ASSOC);

		$this->insertPS = $this->connection->prepare("INSERT INTO register ( title, forename, surname, email, phone, country, city, emailoptin, phoneoptin, source ) VALUES ( :title, :forename, :surname, :email, :phone, :country, :city, :emailoptin, :phoneoptin, :source )");
		$this->insertPS->bindParam(':title', $this->title, PDO::PARAM_STR);
		$this->insertPS->bindParam(':forename', $this->forename, PDO::PARAM_STR);
		$this->insertPS->bindParam(':surname', $this->surname, PDO::PARAM_STR);
		$this->insertPS->bindParam(':email', $this->email, PDO::PARAM_STR);
		$this->insertPS->bindParam(':phone', $this->phone, PDO::PARAM_STR);
		$this->insertPS->bindParam(':country', $this->country, PDO::PARAM_STR);
		$this->insertPS->bindParam(':city', $this->city, PDO::PARAM_STR);
		$this->insertPS->bindParam(':emailoptin', $this->emailoptin, PDO::PARAM_INT);
		$this->insertPS->bindParam(':phoneoptin', $this->phoneoptin, PDO::PARAM_INT);
		$this->insertPS->bindParam(':source', $this->source, PDO::PARAM_STR);

		$this->updatePS = $this->connection->prepare("UPDATE register SET title = :title, forename = :forename, surname = :surname, email = :email, phone = :phone, country = :country, city = :city , emailoptin = :emailoptin, phoneoptin = :phoneoptin WHERE player.id = :id");
		$this->updatePS->bindParam(':id', $this->id, PDO::PARAM_INT);
		$this->updatePS->bindParam(':title', $this->title, PDO::PARAM_STR);
		$this->updatePS->bindParam(':forename', $this->forename, PDO::PARAM_STR);
		$this->updatePS->bindParam(':surname', $this->surname, PDO::PARAM_STR);
		$this->updatePS->bindParam(':email', $this->email, PDO::PARAM_STR);
		$this->updatePS->bindParam(':phone', $this->phone, PDO::PARAM_STR);
		$this->updatePS->bindParam(':country', $this->country, PDO::PARAM_STR);
		$this->updatePS->bindParam(':city', $this->city, PDO::PARAM_STR);
		$this->updatePS->bindParam(':emailoptin', $this->emailoptin, PDO::PARAM_INT);
		$this->updatePS->bindParam(':phoneoptin', $this->phoneoptin, PDO::PARAM_INT);

		$this->deleteByIDPS = $this->connection->prepare("DELETE FROM register WHERE register.id = :id");
		$this->deleteByIDPS->bindParam(':id', $this->id, PDO::PARAM_INT);
		
	}
	
	public function selectAll(){
		$this->selectAllPS->execute();
		return $this->selectAllPS->fetchall();
	}

	public function selectByID( $id ){
		$this->id = $id;
		$this->selectByIDPS->execute();
		return $this->selectByIDPS->fetchall();
	}

	public function insert( $fields ){
		$this->insertPS->execute( $fields );
		return $this->connection->lastInsertId();
	}

	public function update( $fields ){
		$this->updatePS->execute( $fields );
	}

	public function delete( $id ){
		$this->id = $id;
		$this->deleteByIDPS->execute();
	}

};
?>