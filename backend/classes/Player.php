<?php
class Player extends Model{

	private $PDOConnection;
	
	public function __construct(){
		debug( 'Player constructor', 2 );

		parent::__construct();
	}
	
	public function addPlayer($newPlayer){
		debug( "Player-&gt;addPlayer", 2 );

		$this->build( $newPlayer );

		// insert user agent and http referrer info to player properties
		$refererName = ( array_key_exists('HTTP_REFERER', $_SERVER) ) ? $_SERVER['HTTP_REFERER'] : 'NOT SET';
		$this->__set('agent', $_SERVER['HTTP_USER_AGENT']);
		$this->__set('referer', $refererName);

		if($this->validate()){
			$this->save();
			$this->jsonResponse(
				array(
					"id" => $this->__get('id')
				)
			);
		} else {
			$this->jsonResponse(array(
				"error" => "invalid form input"
			));
		}
	}

	public function leaderboard(){
		$this->PDOConnection = new Player_PDO();

		$query = $this->PDOConnection->leaderboard();
		if( $query ){
			$this->jsonResponse($query);
		}
	}

	public function load( $id ){
		debug( "Player-&gt;load $id", 2 );
		$this->PDOConnection = new Player_PDO();
		$query = $this->PDOConnection->selectByID( $id );
		if( $query ){
			$this->build( $query[0] );
			return true;
		}else{
			return false;
		}
	}

	public function topScore(){
		debug( "Player-&gt;topScore", 2 );

		$this->PDOConnection = new Player_PDO();
		$query = $this->PDOConnection->selectTopScore();
		if( $query ){
			return $query[0]["time"];
		}else{
			return 0;
		}
    }
    
    public function save(){
		debug( "Player-&gt;save", 2 );

		debug( "Player-&gt;save :".$this->__get('id'), 2 );

		$newProperties = $this->properties;
		unset( $newProperties['id'] );

		// insert new item
		debug( "Player-&gt;save insert executed", 2 );
		$this->PDOConnection = new Player_PDO();
		$newID = $this->PDOConnection->insert( $newProperties );
		$this->__set( 'id', $newID );

		return $this->__get('id');
	}
	
	public function delete( $id = null ){
		if( !is_null( $id ) ){
			$deleteID = $id;
		}else{
			$deleteID = $this->__get('id');
		}
	}

	public function validate(){
		$isValid = true;

		$testList = array(
			"name" => $this->__get('name'),
			"email" => $this->__get('email'),
			"sector" => $this->__get('sector'),
			"region" => $this->__get('region'),
			"time" => $this->__get('time'));

		// check for empty fields
		foreach( $testList as $key => $field ){
			if( preg_match('/^[\s]*$/' , $field ) ){
				$isValid = false;
			}
		}

		// apply naughty word censor to name field
		$censor = new CensorWords();
		$badwords = $censor->setDictionary('en-uk');
		/*
		$censor->censorString returns an array:
		(
		"orig"
		"clean"
		)
 		*/
		$tempName = $this->__get('name');
		$this->__set('name', $censor->censorString($tempName)["clean"]);
		$this->__set('name2', $censor->censorString($tempName)["orig"]);

		// check to see if time field is valid
		if (!preg_match('/[\d]+$/' , $this->__get('time') )) {
			$isValid = false;
		}

		// generate random time cutoff 60-65 seconds
		$dodgy_time = rand ( 60000, 65000 );
		// mark times above $dodgy_time as 'suspicious' so they can be filtered out of the leaderboard
		if ($this->__get('time') > $dodgy_time && $this->__get('suspicious') == 0) {
			$this->__set('suspicious', 1);
		}

		if (!filter_var($testList["email"], FILTER_VALIDATE_EMAIL)){
			$isValid = false;
		}

		return $isValid;
	}

	public function jsonResponse( $data = null ){
		if ( !isset($data)) {
			$data = $this->properties;
		}

		header('Content-type: application/json');
		echo json_encode( $data );
	}
}
?>
