<?php
class Player_PDO extends Database{


	private $getLatestIDPS;
	private $setLatestIDPS;
	
	public function __construct(){
		parent::__construct();
	
		global $dbConnections;
		$this->connect(array(
			"host" => $dbConnections[DEFAULT_DB]["host"],
			"database" => $dbConnections[DEFAULT_DB]["db"],
			"user" => $dbConnections[DEFAULT_DB]["user"],
			"password" => $dbConnections[DEFAULT_DB]["password"]
		));

		$this->selectByIDPS = $this->connection->prepare("SELECT * FROM player WHERE id = :id");
		$this->selectByIDPS->bindParam(':id', $this->id, PDO::PARAM_INT);
		$this->selectByIDPS->setFetchMode(PDO::FETCH_ASSOC);

		/*
		$this->leaderboardPS = $this->connection->prepare("SELECT name, region, sector, time FROM player WHERE suspicious = 0");
		$this->leaderboardPS->setFetchMode(PDO::FETCH_ASSOC);
		*/

		$this->leaderboardPS = $this->connection->prepare("( SELECT name, 'global' AS ts, region, sector, time FROM player WHERE suspicious = 0 ORDER BY time DESC LIMIT 10 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'uk' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'us' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'belgium' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'australia' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'spain' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'middleeast' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'canada' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'japan' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'china' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'france' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'germany' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'italy' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'switzerland' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'holland' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'luxembourg' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'hongkong' ORDER BY time DESC LIMIT 5 )
UNION
( SELECT name, 'region' AS ts, region, sector, time FROM player WHERE suspicious = 0 AND region LIKE 'region_other' ORDER BY time DESC LIMIT 5 )");
		$this->leaderboardPS->setFetchMode(PDO::FETCH_ASSOC);

		$this->selectTopScorePS = $this->connection->prepare("SELECT time FROM player WHERE suspicious = 0 ORDER BY time DESC LIMIT 1");
		$this->selectTopScorePS->setFetchMode(PDO::FETCH_ASSOC);
        
		$this->leaderboardregionPS = $this->connection->prepare("SELECT name, region, sector, time FROM player  WHERE region LIKE :region ORDER BY time DESC");
		$this->leaderboardregionPS->bindParam(':region', $this->region, PDO::PARAM_STR);
		$this->leaderboardregionPS->setFetchMode(PDO::FETCH_ASSOC);

		$this->insertPS = $this->connection->prepare("INSERT INTO player ( name, name2, email, region, sector, time, language, suspicious, agent, referer ) VALUES ( :name, :name2, :email, :region, :sector, :time, :language, :suspicious, :agent, :referer )");
		$this->insertPS->bindParam(':name', $this->name, PDO::PARAM_STR);
		$this->insertPS->bindParam(':name2', $this->name2, PDO::PARAM_STR);
		$this->insertPS->bindParam(':email', $this->email, PDO::PARAM_STR);
		$this->insertPS->bindParam(':region', $this->region, PDO::PARAM_STR);
		$this->insertPS->bindParam(':sector', $this->sector, PDO::PARAM_STR);
		$this->insertPS->bindParam(':time', $this->time, PDO::PARAM_INT);
		$this->insertPS->bindParam(':language', $this->language, PDO::PARAM_STR);
		$this->insertPS->bindParam(':suspicious', $this->suspicious, PDO::PARAM_INT);
		$this->insertPS->bindParam(':agent', $this->agent, PDO::PARAM_STR);
		$this->insertPS->bindParam(':referer', $this->referer, PDO::PARAM_STR);

		$this->updatePS = $this->connection->prepare("UPDATE player SET name = :name, name2 = :name2, email = :email, region = :region, sector = :sector, time = :time, language = :language  WHERE player.id = :id");
		$this->updatePS->bindParam(':id', $this->id, PDO::PARAM_INT);
		$this->updatePS->bindParam(':name', $this->name, PDO::PARAM_STR);
		$this->updatePS->bindParam(':name2', $this->name2, PDO::PARAM_STR);
		$this->updatePS->bindParam(':email', $this->email, PDO::PARAM_STR);
		$this->updatePS->bindParam(':region', $this->region, PDO::PARAM_STR);
		$this->updatePS->bindParam(':sector', $this->sector, PDO::PARAM_STR);
		$this->updatePS->bindParam(':time', $this->time, PDO::PARAM_INT);
		$this->updatePS->bindParam(':language', $this->language, PDO::PARAM_STR);

		$this->deleteByIDPS = $this->connection->prepare("DELETE FROM player WHERE player.id = :id");
		$this->deleteByIDPS->bindParam(':id', $this->id, PDO::PARAM_STR);
		
	}
	
	public function selectByID( $id ){
		$this->id = $id;
		$this->selectByIDPS->execute();
		return $this->selectByIDPS->fetchall();
	}

    public function selectTopScore(){
		$this->selectTopScorePS->execute();
		return $this->selectTopScorePS->fetchall();
	}

	public function insert( $fields ){
		$this->insertPS->execute( $fields );
		return $this->connection->lastInsertId();
	}

	public function update( $fields ){
		$this->updatePS->execute( $fields );
	}

	public function delete( $id ){
		$this->id = $id;
		$this->deleteByIDPS->execute();
	}

	public function leaderboard(){
		$this->leaderboardPS->execute();
		return $this->leaderboardPS->fetchall();
	}

	public function leaderboardregion( $region ){
			$this->region = $region;
		$this->leaderboardregionPS->execute();
		return $this->leaderboardregionPS->fetchall();
	}

};
?>