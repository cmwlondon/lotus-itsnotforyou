<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);

class Register extends Model{

	private $PDOConnection;
	
	public function __construct(){
		debug( 'Register constructor', 2 );

		parent::__construct();
	}
	
	public function addRegister($newRegister){
		debug( "Register-&gt;addRegister", 2 );


		// $this->build( $newRegister );

		$this->build( array(
			"id" => -1,
			"title" => $newRegister['title'],
			"forename" => $newRegister['forename'],
			"surname" => $newRegister['surname'],
			"email" => $newRegister['email'],
			"phone" => $newRegister['phone'],
			"country" => $newRegister['country'],
			"city" => $newRegister['city'],
			"emailoptin" => $newRegister['emailoptin'],
			"phoneoptin" => $newRegister['phoneoptin'],
			"source" => $newRegister['utmsource'],
		) );
		if($this->validate()){
			$this->save();
			$saleforce = $this->saleforceSubmit($newRegister['utmsource']);			
			$saleforce["id"] = $this->__get("id");
			$this->jsonResponse($saleforce);
		} else {
			$this->jsonResponse(array(
				"error" => "invalid form input"
			));
		}
	}

	public function load( $id ){
		debug( "Register-&gt;load $id", 2 );
		$this->PDOConnection = new Register_PDO();
		$query = $this->PDOConnection->selectByID( $id );
		if( $query ){
			$this->build( $query[0] );
			return true;
		}else{
			return false;
		}
	}

    public function save(){
		debug( "Register-&gt;save", 2 );

		debug( "Register-&gt;save :".$this->__get('id'), 2 );

		if( $this->__get('id') == -1){
			debug( "Register-&gt;save insert", 2 );
			// trim off id property
			$newProperties = $this->properties;
			unset( $newProperties['id'] );
	
			// insert new item
			debug( "Register-&gt;save insert executed", 2 );
			$this->PDOConnection = new Register_PDO();
			$newID = $this->PDOConnection->insert( $newProperties );
			$this->__set( 'id', $newID );
		}else{
			debug( "Register-&gt;save update", 2 );
			// update existing item
			$this->PDOConnection = new Register_PDO();
 			$userQuery = $this->PDOConnection->update( $this->properties ); 
		}
		return $this->__get('id');
	}
	
	public function delete( $id = null ){
		if( !is_null( $id ) ){
			$deleteID = $id;
		}else{
			$deleteID = $this->__get('id');
		}
	}

	public function validate(){
		$isValid = true;

		$testList = array(
			"title" => $this->__get('title'),
			"forename" => $this->__get('forename'),
			"surname" => $this->__get('surname'),
			"country" => $this->__get('country'),
			"city" => $this->__get('city'),
			"email" => $this->__get('email'),
			"phone" => $this->__get('phone'),
			"emailoptin" => $this->__get('emailoptin'),
			"phoneoptin" => $this->__get('phoneoptin')
		);

		// check for empty fields
		foreach( $testList as $key => $field ){
			if( preg_match('/^[\s]*$/' , $field ) ){
				$isValid = false;
			}
		}

		if (!filter_var($testList["email"], FILTER_VALIDATE_EMAIL)){
			$isValid = false;
		}

		// validate phone number - need to find decent international phone number REGEX

		return $isValid;
	}

	public function saleforceSubmit($utmsource){
		$parameters = array(
			"title" => $this->__get('title'),
			"forename" => $this->__get('forename'),
			"surname" => $this->__get('surname'),
			"country" => $this->__get('country'),
			"city" => $this->__get('city'),
			"email" => $this->__get('email'),
			"phone" => $this->__get('phone'),
			"emailoptin" => $this->__get('emailoptin'),
			"phoneoptin" => $this->__get('phoneoptin')
		);

        $debug = false;
        $debugEmail = 'matt.klippel@stackworks.com';
       
        //build up a URL and querystring to send to Salesforce for web2lead
        $salesforceURL = 'https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
        $salesforceInstance = '00DD0000000CKDF';  //live //ONLY LIVE ACCOUNT CAN BE ACCESSED
        // 00Dd0000000d0TB Dev account
        // 00DL0000000BffV Lotus sandbox
        // 00DD0000000CKDF Lotus live
       
        //start building up the querystring params
        $salesforceQS = '';          
        $salesforceQS .= 'oid=' . $salesforceInstance;
        $salesforceQS .= '&00ND0000003b8LC=E400'; //interest code, value TBC
        $salesforceQS .= '&00ND0000003cYqD=' . date_format(new DateTime(), 'd/m/Y');
        $salesforceQS .= '&00ND0000002s0Op=' . $parameters["emailoptin"]; //0=No,1=Yes
       
        if($parameters["country"] == 'gb'){
                        $salesforceQS .= '&lea1=005D0000001b04T';
        }
       
        $salesforceQS .= '&country=' . $parameters["country"];
        $salesforceQS .= '&city=' . urlencode ( $parameters["city"] );
        $salesforceQS .= '&title=' . $parameters["title"];
        $salesforceQS .= '&first_name=' . $parameters["forename"];
        $salesforceQS .= '&last_name=' . $parameters["surname"];
        $salesforceQS .= '&phone=' . urlencode ( $parameters["phone"] );
        $salesforceQS .= '&email=' . $parameters["email"];
        $salesforceQS .= '&00ND0000003b8LH=' . $parameters["phoneoptin"]; //0=No,1=Yes
        $salesforceQS .= '&00ND0000003b8L7=' . $parameters["emailoptin"]; //0=No,1=Yes

        // $salesforceQS .= '&description=E400%20ITSNOTFORYOU%20Campaign&00ND0000003b8LG=no&00ND0000002s1rd=1'; //description / testdrive=no / more info=yes
        $salesforceQS .= '&description=E400%20ITSNOTFORYOU%20Campaign';

       	// append value to end of description parameter depending on values of sourceFlag and source pulled from traacking tag on app URL
       	if ($utmsource != '') {
       		$salesforceQS .= '_';
       		$salesforceQS .= $utmsource;
       	}
        $salesforceQS .= '&00ND0000003b8LG=no&00ND0000002s1rd=1'; //description / testdrive=no / more info=yes
       

        if($debug){
                        $salesforceQS .= '&debug=1&debugEmail=' . $debugEmail;
        }
       
        // echo $salesforceQS;
       
        //send data using CURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $salesforceURL);
        //curl_setopt($curl, CURLOPT_PORT , 443);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $salesforceQS);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_USERAGENT, "lotus_salesforce module for Drupal");

        /* This line actually executes the CURL request - comment out/disable for testing */
        if( !SALESFORCE_TEST ){
        	$response = curl_exec($curl);
        } else {
        	$response = 'TEST MODE';
        }
        /**/

        /*
        echo 'curl response: ' + $response;

        if (curl_error($curl)) {
                        //handle error
                        echo 'curl error no: ' . curl_errno($curl);
                        echo 'curl error: ' . curl_error($curl);
        }
        curl_close($curl);
        */
        return array(
        	"url" => $salesforceQS,
        	"response" => $response
        );
	}

	public function jsonResponse( $data = null ){
		if ( !isset($data)) {
			$data = $this->properties;
		}

		header('Content-type: application/json');
		echo json_encode( $data );
	}
}
?>
