<?php

include("CensorWords.php");

use Snipe\BanBuilder\CensorWords;


$wordToTest = 'arsegoblin'; //matches @rse, 4rse etc.

$censor = new CensorWords;
$badwords = $censor->setDictionary('en-uk');
$string = $censor->censorString($wordToTest);

echo 'clean = ' . $string['clean'] . '<br />';
echo 'orig = ' . $string['orig'] . '<br />';


?>