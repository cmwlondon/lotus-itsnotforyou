<?php
// ?debug=8817
if( array_key_exists('debug',$_GET) && $_GET['debug'] = 8817 ){
	$debugEnabled = true;
}else{
	$debugEnabled = false;
}

function __autoload($className){
	if( file_exists( CLASSES."generic/".$className.".php" ) ){
		// handle generic classes
		require_once CLASSES."generic/".$className.".php";
		return;
	}else{
		// handle namespaced classes
		// convert complex classname to include file path
		// CLASSES/[Namespace]/[Class] = [Namespace]_[Class]

		$classPath = CLASSES.$className.'.php';
		if( file_exists( $classPath ) ){
			require_once $classPath;
			return;
		}
		
	}
}

function debug( $message, $level ){
	if( DEBUG )
	// global $debugEnabled;
	// if( $debugEnabled )
		echo "<p class=\"level$level\">$message</p>\n";
}

function loadTemplate( $template ){
	$template = TEMPLATES.$template;
	if( file_exists( $template ) ){
		$handle = fopen( $template, "r");
		$loadedTemplate = fread($handle, filesize( $template ));
		fclose($handle);
		return $loadedTemplate;
	}else{
		false;
	}
}

function propertyInsert( $template, $values ){
	$output = $template;
	foreach( $values as $key => $value ){
		$output = str_replace("[$key]", $value, $output );
	}
	return $output;
}

function bytes($bytes, $force_unit = NULL, $format = NULL, $si = TRUE)
{
    // Format string
    $format = ($format === NULL) ? '%01.2f %s' : (string) $format;

    // IEC prefixes (binary)
    if ($si == FALSE OR strpos($force_unit, 'i') !== FALSE)
    {
        $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
        $mod   = 1024;
    }
    // SI prefixes (decimal)
    else
    {
        $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
        $mod   = 1000;
    }

    // Determine unit to use
    if (($power = array_search((string) $force_unit, $units)) === FALSE)
    {
        $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
    }

    return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
}

// handle tweet IDs as strings, compare digit by digit
function tweetIDCompare( $id1, $id2 ){
	$stringLength = strlen( $id1 );
	if( ( strlen( $id1 ) == strlen( $id2 ) ) && preg_match( '/^[0-9]+$/', $id1 ) && preg_match( '/^[0-9]+$/', $id2 ) ){
		// strings are same length and are both numeric

		for($n = 0; $n < $stringLength;$n++){
			$twComp = digitCompare( substr( $id1, $n ,1 ), substr( $id2, $n ,1 ) );
			if( $twComp != 0 ){
				return $twComp;
			}
		}
		return 0; // id1 = id2
	}else{
		if( strlen( $id1 ) > strlen( $id2 ) ){
			return 1;
		}else{
			return -1;
		}
	}
}

function digitCompare( $a, $b ){
	$v = ((int) $a) - ((int) $b);
	if( $v < 0)
		return -1;
	if( $v == 0)
		return 0;
	if( $v > 0)
		return 1;
}
?>