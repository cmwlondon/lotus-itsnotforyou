<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);

	require '../config.php';
	require_once '../include/sqAES.php';
	require_once '../include/JCryption.php';

	class PlayerParameters{
		private $properties;

		public $id;
		public $time;
		public $name;
		public $email;
		public $region;
		public $sector;
		public $language;

		public function __construct(){
			$this->properties = array();
		}
	};

	$testmode = false;
    if (array_key_exists("testmode",$_POST)) {
	    if ($_POST["testmode"] === 'html') {
			$testmode = true;
	    }
	}

    // angular sends POST variables as JSON object, need to parse the JSON to get the individual form items
    
	$process = $_SERVER['REQUEST_URI'];
	// echo $process;
	$uriArray = preg_split('/\//', $process, -1, PREG_SPLIT_NO_EMPTY);
	// print_r($uriArray);

	// $mode = $uriArray[0];
	// /itsnotforyou_sandbox/player/topscore/
	// /itsnotforyou_sandbox/player/save/
	// /itsnotforyou_sandbox/leaderboard/
	// /itsnotforyou_sandbox/register/
	/*
	// subdirectory /itsnotforyou/
	$modeP = 1;
	$actionP = 2;
	$paramP = 3;
	*/

	// root directory /
	$modeP = 0;
	$actionP = 1;
	$paramP = 2;

	$mode = $uriArray[$modeP];

	switch($mode){
		case "player" : {

			$thisPlayer = new Player();

		    if ($testmode) {
		    	// html test form
		    	// map $_POST variables to $param properties

		    	$params = new PlayerParameters();
				$params->id = $_POST["id"];
				//$params->id = '-1';
				$params->time = $_POST["time"];
				$params->name = $_POST["name"];
				$params->email = $_POST["email"];
				$params->region = $_POST["region"];
				$params->sector = $_POST["sector"];
				$params->language = $_POST["language"];
		    } else {
		    	// AJAX call from angular
		    	$params = json_decode(file_get_contents('php://input'));
		    }

			if ( isset($uriArray[$actionP])) {
				$action = $uriArray[$actionP];
				switch($action){
					case "save" : {
						$jc = new JCryption('../rsa_1024_pub.pem', '../rsa_1024_priv.pem');
						$jc->go();

                        $newPlayer = array(
							"id" =>  -1,
							"time" => $_POST["time"],
							"name" => $_POST["name"],
							"email" => $_POST["email"],
							"region" => $_POST["region"],
							"sector" => $_POST["sector"],
							"language" => $_POST["language"]
						);
                        
						$thisPlayer->addPlayer($newPlayer); // return new player object to client side as JSON object
					} break;
					case "update" : {
                        // update the score (time) of an existing player (id)
                        $thisPlayer->load($params->id);
                        $thisPlayer->__set( "time", $params->time);
                        $thisPlayer->save();
					} break;
					case "load" : {
						if ( isset($uriArray[$paramP])) {
							if (is_numeric($uriArray[$paramP])) {
								 // load player
								if ($thisPlayer->load($uriArray[$paramP]) ) {
									$thisPlayer->jsonResponse();
								} else {
									$thisPlayer->jsonResponse(array("error" => "player not found"));
								}
							} else {
								// invalid player id
								$thisPlayer->jsonResponse(array("error" => "invalid player id"));
							}
						} else {
							// player id not supplied
							$thisPlayer->jsonResponse(array("error" => "player id not specified"));
						}
					} break;
					case "topscore" : {
                        // get the current world top score
                        
                        $thisPlayer->jsonResponse(array(
                            "topscore" => $thisPlayer->topScore()
                        ));
					} break;
					case "test" : {
                        // get the current world top score
                        // phpinfo();

						echo "<p>host: ".$dbConnections[DEFAULT_DB]["host"]." / db: ".$dbConnections[DEFAULT_DB]["db"]." / user: ".$dbConnections[DEFAULT_DB]["user"]." / password:".$dbConnections[DEFAULT_DB]["password"]."</p>\n";
$connection = new PDO("mysql:host=".$dbConnections[DEFAULT_DB]["host"].";dbname=".$dbConnections[DEFAULT_DB]["db"], $dbConnections[DEFAULT_DB]["user"], $dbConnections[DEFAULT_DB]["password"] );
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$connection->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
$status = true;

					} break;
					default : {
					}
				}
			} else {
			}

		} break;
		case "leaderboard" : {
			// load leaderboard
			$thisPlayer = new Player();
			$thisPlayer->leaderboard();
		} break;
		case "register" : {
			// save data from 'register your interest' form
		    if ($testmode) {
		    	// html test form
				$newRegister = array(
					"id" => -1,
					"title" => $_POST["title"],
					"forename" => $_POST["forename"],
					"surname" => $_POST["surname"],
					"email" => $_POST["email"],
					"phone" => $_POST["phone"],
					"country" => $_POST["country"],
					"city" => $_POST["city"],
					"emailoptin" => (isset($_POST["emailoptin"])) ? $_POST["emailoptin"] : 1,
					"phoneoptin" => (isset($_POST["phoneoptin"])) ? $_POST["phoneoptin"] : 1
				);
		    } else {
		    	// AJAX call from angular
		    	$params = json_decode(file_get_contents('php://input'));
				$newRegister = array(
					"id" => -1,
					"title" => $params->title,
					"forename" => $params->forename,
					"surname" => $params->surname,
					"email" => $params->email,
					"phone" => $params->phone,
					"country" => $params->country,
					"city" => $params->city,
					"emailoptin" => $params->emailoptin,
					"phoneoptin" => $params->phoneoptin,
					"utmsource" => $params->utmsource
				);

				// $newRegister =  (array) $params;
		    }

		   // make sure that if a registration id has not been supplied, the id passed to the Register object is always -1, i.e. a brand new registration

			$thisRegister = new Register();
			$thisRegister->addRegister($newRegister); // return new player object to client side as JSON object
		} break;
		default : {

		}
	}
?>