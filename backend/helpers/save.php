<?php
	require '../config.php';
	require_once '../include/sqAES.php';
	require_once '../include/JCryption.php';

	if ( array_key_exists('jCryption', $_POST) ) {
		$jc = new JCryption('../rsa_1024_pub.pem', '../rsa_1024_priv.pem');
		$jc->go();
		// jCrypiton decrypts form data and populates $_POST
		$thisPlayer = new Player();
	    $newPlayer = array(
			"id" =>  -1,
			"time" => $_POST["time"],
			"name" => $_POST["name"],
			"email" => $_POST["email"],
			"region" => $_POST["region"],
			"sector" => $_POST["sector"],
			"language" => $_POST["language"],
			"suspicious" => $_POST["pflag"]
		);
	    
		$thisPlayer->addPlayer($newPlayer); // return new player object to client side as JSON object
	}
?>