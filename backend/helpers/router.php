<?php
	require '../config.php';

	$testmode = false;
    if (array_key_exists("testmode",$_POST)) {
	    if ($_POST["testmode"] === 'html') {
			$testmode = true;
	    }
	}

    // angular sends POST variables as JSON object, need to parse the JSON to get the individual form items
    
	$process = $_SERVER['REQUEST_URI'];
	// echo $process;
	$uriArray = preg_split('/\//', $process, -1, PREG_SPLIT_NO_EMPTY);
	// print_r($uriArray);

	// $mode = $uriArray[0];
	// /itsnotforyou_sandbox/player/topscore/
	// /itsnotforyou_sandbox/player/save/
	// /itsnotforyou_sandbox/leaderboard/
	// /itsnotforyou_sandbox/register/
	/*
	// subdirectory /itsnotforyou/
	$mode = ( array_key_exists(1, $uriArray)) ? $uriArray[1] : '';
	$action = ( array_key_exists(2, $uriArray)) ? $uriArray[2] : '';
	*/

	// (open shift environment) root directory /
	$mode = ( array_key_exists(0, $uriArray)) ? $uriArray[0] : '';
	$action = ( array_key_exists(1, $uriArray)) ? $uriArray[1] : '';

	switch($mode){
		case "player" : {

			$thisPlayer = new Player();

			if ( isset($action)) {
				switch($action){
					/*
					case "save" : {
					} break;
					*/
					case "topscore" : {
                        // get the current world top score
                        
                        $thisPlayer->jsonResponse(array(
                            "topscore" => $thisPlayer->topScore()
                        ));
					} break;
					case "xylophone" : {
						phpinfo();
					} break;
					default : {
					}
				}
			} else {
			}

		} break;
		case "leaderboard" : {
			// load leaderboard
			$thisPlayer = new Player();
			$thisPlayer->leaderboard();
		} break;
		case "register" : {
			// save data from 'register your interest' form
		    if ($testmode) {
		    	// html test form
				$newRegister = array(
					"id" => -1,
					"title" => $_POST["title"],
					"forename" => $_POST["forename"],
					"surname" => $_POST["surname"],
					"email" => $_POST["email"],
					"phone" => $_POST["phone"],
					"country" => $_POST["country"],
					"city" => $_POST["city"],
					"emailoptin" => (isset($_POST["emailoptin"])) ? $_POST["emailoptin"] : 1,
					"phoneoptin" => (isset($_POST["phoneoptin"])) ? $_POST["phoneoptin"] : 1
				);
		    } else {
		    	// AJAX call from angular
		    	$params = json_decode(file_get_contents('php://input'));
				$newRegister = array(
					"id" => -1,
					"title" => $params->title,
					"forename" => $params->forename,
					"surname" => $params->surname,
					"email" => $params->email,
					"phone" => $params->phone,
					"country" => $params->country,
					"city" => $params->city,
					"emailoptin" => $params->emailoptin,
					"phoneoptin" => $params->phoneoptin,
					"utmsource" => $params->utmsource
				);

				// $newRegister =  (array) $params;
		    }

		   // make sure that if a registration id has not been supplied, the id passed to the Register object is always -1, i.e. a brand new registration

			$thisRegister = new Register();
			$thisRegister->addRegister($newRegister); // return new player object to client side as JSON object
		} break;
		default : {

		}
	}
?>