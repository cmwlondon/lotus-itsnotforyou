<?php
	require '../config.php';

	$testmode = false;
    if (array_key_exists("testmode",$_POST)) {
	    if ($_POST["testmode"] === 'html') {
			$testmode = true;
	    }
	}

    if ($testmode) {
    	// html test form
    	// map $_POST variables to $param properties

 	    $newPlayer = array(
			"id" => $_POST["id"],
			"name" => $_POST["name"],
			"email" => $_POST["email"],
			"region" => $_POST["region"],
			"sector" => $_POST["sector"],
			"time" => $_POST["time"],
			"language" => $_POST["language"]
		);
    } else {
    	// AJAX call from angular
    	$params = json_decode(file_get_contents('php://input'));

	    $newPlayer = array(
			"id" => $params->id,
			"name" => $params->name,
			"email" => $params->email,
			"region" => $params->region,
			"sector" => $params->sector,
			"time" => $params->time,
			"language" => $params->language
		);
    }

	$thisPlayer = new Player();

	$thisPlayer->addPlayer($newPlayer);
	$thisPlayer->jsonResponse(); // return new player object to client side as JSON object
?>