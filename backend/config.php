<?php
require_once('utils.php');
// use Snipe\BanBuilder\CensorWords;

date_default_timezone_set('Europe/London');

define( "DEBUG", false );
define( "DEV", true );

define("SALESFORCE_TEST", FALSE);

// staging
//define( "PATHROOT", $_SERVER["DOCUMENT_ROOT"]."/itsnotforyou_sandbox/backend/" );

// LIVE
// define( "PATHROOT", $_SERVER["DOCUMENT_ROOT"]."/itsnotforyou/backend/" );
define( "PATHROOT", $_SERVER["DOCUMENT_ROOT"]."/backend/" );

define( "CLASSES", PATHROOT."classes/" );

define("USER_SESSION", "userid" );
define("SALT_LENGTH", 32 );

define('DEFAULT_DB','lotus');

define('DODGY_TIME', 65535); // deal with morons faking posts with absurd time values, filter out all times above this value

// databse connections for various apps
// host: 127.10.181.2 
// port: 3306 
// root user: adminyySmech
// root password: C_sZrPruSbDA
// db name: itsnotforyou
/*if ( getenv('OPENSHIFT_GEAR_NAME') ) {
	$dbConnections = array(
		"lotus" => array(
			"db" => getenv('OPENSHIFT_GEAR_NAME'),
			"host" => getenv('OPENSHIFT_MYSQL_DB_HOST'),
			"user" => getenv('OPENSHIFT_MYSQL_DB_USERNAME'),
			"password" => getenv('OPENSHIFT_MYSQL_DB_PASSWORD'),
		)
	);
}else{
	$dbConnections = array(
		"lotus" => array(
			"db" => "lotus",
			"host" => "localhost",
			"user" => "lotus",
			"password" => "fastCarChallenge",
		)
	);
}
*/

/*
$dbConnections = array(
	"lotus" => array(
		"db" => "c_stack_e400",
		"host" => "192.168.200.66",
		"user" => "stackworks",
		"password" => "Abcd1234bcdA",
	)
);
*/
$dbConnections = array(
	"lotus" => array(
		"db" => "lotus",
		"host" => "localhost",
		"user" => "lotus",
		"password" => "fastCarChallenge",
	)
);
?>
