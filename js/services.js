/* services:
gameState[
    gameState.addState(key, value)
    gameState.getState()
]
*/

app.service('gameState', function() {
  var gameState = [];

  var addState = function(key, value) {
      gameState[key] = value;
  }

  var getState = function(){
      return gameState;
  }

  return {
    addState: addState,
    getState: getState
  };

});
