// format leaderboard time values into SS.MM
angular.module('statemachine').filter('prettyTime', function () {
    return function (time) {
        var tTime = formatTime(time);
        return tTime.seconds + "." + tTime.milliseconds;
    };
});

