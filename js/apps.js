
/* app */
// statemachine

/*
directives:

languageSelection
mobileMenu
lgFunkyDropDown
*/

/*
include module dependencies and do peliminary setup
*/
var timeThreshold = 7000, // time in milliseconds above which the player's score is recorded
    gameTestMode = false; // disable collision checking in game for testing purposes

// staging
//var baseURL = '/itsnotforyou_sandbox/';

// LIVE
var baseURL = '/itsnotforyou/';
var baseURL = '/';
var controllBox = 'wkluerj97jf598d27';

var gameIsInstalled = false,
    thisAgilityGame,
    pageFadeDone = false;

var translationJSON = ['main_en.json','main_de.json','main_fr.json','main_it.json','main_nl.json'];

var app = angular.module('statemachine', ['ui.router', 'pascalprecht.translate', 'ngCookies', 'ngAnimate']);

app.config(['$httpProvider', '$translateProvider', '$translatePartialLoaderProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider',  function($httpProvider, $translateProvider, $translatePartialLoaderProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

    // define loader URL for translations
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate : 'js/{part}_{lang}.json'
    });

    // load english tranlsation
    $translatePartialLoaderProvider.addPart('main');
    $translateProvider.preferredLanguage('en');

 
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};    
    }    
     //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    // ui.router setting - default page
    $urlRouterProvider.otherwise('game');

    // ui-router states
    $stateProvider
        .state('game', {
            url:'/',
            templateUrl: 'templates/game.html',
            controller: 'gamePanel'
        })
        .state('form', {
            url: '/form',
            templateUrl: 'templates/form.html',
            controller: 'gameForm'
        })
        .state('share', {
            url: '/share',
            templateUrl: 'templates/share.html',
            controller: 'gameShare'
        })
        .state('register', {
            url: '/register',
            templateUrl: 'templates/register.html',
            controller: 'register'
        })
        .state('thanks', {
            url: '/thanks',
            templateUrl: 'templates/register_thanks.html',
            controller: 'thanks'
        })
        .state('leaderboard', {
            url:'/leaderboard',
            templateUrl: 'templates/leaderboard.html',
            controller: 'leaderboardPanel'
        })
        .state('reset', {
            url: '/reset',
            templateUrl: 'templates/reset.html',
            controller: 'reset'
        });
}]);

app.run(function ($state, $rootScope, $location, $cookies, $translate, gameState) {

    var defaultLanguage = (typeof $cookies.language !== 'undefined') ? $cookies.language : 'en';
    $translate.use(defaultLanguage);

    $rootScope.$on('$stateChangeSuccess', function(){
        ga('send', 'pageview', $location.path());
    });

    // $rootScope.$on('$includeContentLoaded', function() {});

    // look for tracking link from banner on another site
    var sourcePattern = /utm\_source\=([a-zA-Z0-9\_\%\+]+)/;
    var results = sourcePattern.exec($location.absUrl());
    if (results) {
        console.log("regex match: '%s'", decodeURIComponent(results[1]));
        gameState.addState("sourceFlag", true);
        gameState.addState("source", results[1]);
    } else {
        gameState.addState("sourceFlag", false);
        gameState.addState("source", '');
    }

    $state.go('game');
});

// build and activate language selection dropdown
app.directive('languageSelection', function() {
    return {
        restrict: 'A',
        replace: 'true',
        templateUrl: 'templates/languageSelector.html',
        controller: ['$rootScope', '$scope',  '$translate', '$cookies', function($rootScope, $scope, $translate, $cookies) {

            $scope.ngLanguage = (typeof $cookies.language !== 'undefined') ? $cookies.language : 'en';
            $translate.use($scope.ngLanguage);

            $scope.updateLangauge = function (newLanguage) {
                $scope.ngLanguage = newLanguage;
                $cookies.language = $scope.ngLanguage;
                $translate.use(newLanguage); 
            };
        }],
        link: function(scope, iElement, iAttrs, ctrl) {
            // handle language selector behaviour
            $('.languageSelector li').on('click', function(){
                if (languageSelectorIsOpen){
                    var nlid = $(this).attr('id');
                    // set new language as current
                    scope.ngLanguage = nlid.substr(5, nlid.length - 1);
                    scope.$apply();

                    // close language selector
                    $('.languageSelector').removeClass('open');
                    languageSelectorIsOpen = false;
                } else {
                    // open language selector
                    $('.languageSelector').addClass('open');
                    languageSelectorIsOpen = true;
                }
            });
            scope.$watch('ngLanguage', function(nlid) {
                scope.updateLangauge(nlid);
            });

        }
    };
});

// build page header / navigation
app.directive('mobileMenu', function() {
    return {
        restrict: 'A',
        replace: 'false',
        // templateUrl: 'templates/mobileMenu.html',
        controller: ['$scope', function($scope) {
        }],
        link: function(scope, iElement, iAttrs, ctrl) {
            /* mobile menu behaviour */
            pageHeaderNode = $('header.page');

            // toggle menu using 'hamburger' button
            $('.mobileMenuToggle').on('click', function(e){
                if (pageHeaderNode.hasClass('open')) {
                    pageHeaderNode.removeClass('open');
                } else {
                    pageHeaderNode.addClass('open');
                }
            });
            // close menu when user selects a link
            $('header.page nav a').on('click', function(e){
                pageHeaderNode.removeClass('open');
            });

            $('.lotusLogo').on('click', function(e){
               pageHeaderNode.removeClass('open'); 
            })

        }
    };
});

// http://stackoverflow.com/questions/13294507/two-way-data-binding-in-angularjs-directives
// custom drodown widget for registration and post to leaderboard formds
app.directive('lgFunkyDropDown', function($parse, $document) {
    return {
        require: "ngModel",
        restrict: 'A',
        replace: 'true',
        templateUrl: 'templates/funkydropdown.html',
        scope : {
            "ngModel" : "=ngModel", // bind to parent scope objects refernced in ngModel attribute, 2-way bind
            "options" : "=options", // bind to parent scope objects refernced in ngModel attribute, 2-way bind
            "validationFlag" : "=validationFlag", // bind to parent scope objects refernced in ngModel attribute, 2-way bind
            "labeltr" : "@", // read contents of attribute as string, 1-way bind into directive
            "placeholdertr" : "@", // read contents of attribute as string, 1-way bind into directive
            "errortr" : "@", // read contents of attribute as string, 1-way bind into directive
            "tabIndex" : "@tabIndex",
            "suffix" : "@"
        },
        controller: ['$scope', '$rootScope', '$translate', function($scope, $rootScope, $translate) {
            $scope.currentItem = {};
            $scope.currentItem.id = "PLACEHOLDER";
            $scope.currentItem.tr = $scope.placeholdertr;
            $scope.ngModel = {"id" : ""};

            $scope.labelUpdate = function (newValue) {
                $scope.currentItem.id = newValue.id;
                $scope.currentItem.tr = newValue.tr;
            };

            // handle case where number of options changes after an option has been selected
            // if the selected option is not in the new set, reset the dropdown
            $scope.$watch("options",function(){
                var matchFound = false,
                    optionIndex = 0;

                // find currently selected item in new option set
                while (optionIndex < $scope.options.length && !matchFound) {
                    if ( $scope.ngModel.id === $scope.options[optionIndex].id) {
                        matchFound = true;
                    }
                    optionIndex++;
                }
                // not found, reset the label box to show the placeholder
                if (!matchFound ) {
                    $scope.labelUpdate({"id" : "PLACEHOLDER", "tr" : $scope.placeholdertr});
                    $scope.ngModel = {"id" : ""};
                }
            });
        }],
        link: function ( scope, element, attributes, ngModelController ) {
            var dom = {
                "module" : element,
                "container" : element.find( "div.m-dropdown" ),
                "root" : element.find( "div.dropdown-root" ),
                "rootLabel" : element.find( "div.dropdown-root div.dropdown-label" ),
                "options" : element.find( "ul.dropdown-options" ),
                "errorMessage" : element.find( "p.error" )
            };

            // show/hide error messages
            function ngShow() {
                if (scope.validationFlag === false) {
                    dom.errorMessage.removeClass('invalid');
                }
                else if(scope.validationFlag === true) {
                    dom.errorMessage.addClass('invalid');
                }
            }
            scope.$watch("validationFlag", ngShow);
            setTimeout(ngShow, 0);

            function getOptionValue( option ) {
                var accessor = $parse( option.attr( "option" ) || "null" );
                return( accessor( option.scope() ) );
            }

            // close dropdown if user clicks anywhwew else in page
            $document.on( "mousedown", function(event){
                var target = angular.element( event.target );
                if ( dom.container.hasClass( "dropdown-open" ) && ! target.closest( dom.module ).length ) {
                    dom.container.removeClass('dropdown-open');
                }
            } );

            // open/close dropdown by clicking on label
            dom.root.on('click', function(e){
                if (dom.container.hasClass('dropdown-open')) {
                    // close
                    dom.container.removeClass('dropdown-open');
                } else {
                    // open
                    // apply jScrollPane just bfore opening the dropdown
                    var jSCrollPaneSettings = {
                        showArrows : false,
                        verticalDragMinHeight : 50,
                        verticalDragMaxHeight : 50,
                        autoReinitialise: true
                    };
                    var pane = $('.scroll-pane');
                    pane.jScrollPane(jSCrollPaneSettings);
                    var api = pane.data('jsp');
                    api.reinitialise();

                    dom.container.addClass('dropdown-open');
                }
            });

            // update selected option by clicking on options in open dropdown
            dom.options.on( "click", "li.dropdown-option", function(e){
                dom.options.find('.dropdown-selection').removeClass('dropdown-selection');
                $(this).addClass('dropdown-selection');

                // update ngModel value and label with selected value
                scope.$apply(
                    function changeModel() {
                        var option = angular.element( e.target ),
                            newValue = getOptionValue( option );

                        ngModelController.$setViewValue(newValue);
                        scope.labelUpdate(newValue);
                    }
                );
                dom.container.removeClass('dropdown-open');
            });
        }
    };
});
