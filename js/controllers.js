/*
controllers: 
statemachine
[
	leaderboardPanel
	gamePanel
    gameForm
    gameShare
    register
    thanks
    reset
]
*/

app.controller('leaderboardPanel', ['$scope', '$http', function($scope, $http) {
	// set up translations for text content

    $scope.regions = ['uk', 'us', 'belgium', 'australia', 'spain', 'middleeast', 'canada', 'japan', 'china', 'france', 'germany', 'italy', 'switzerland', 'holland', 'luxembourg', 'hongkong', 'region_other'];
	$http.get(baseURL + 'leaderboard/').success(function( data ) {
		$scope.players = data;
		
		// force conversion of time from string to integer
		/*
		angular.forEach($scope.players, function (player) {
			player.time = parseInt(player.time);
		});
		*/

	});

    // fetch world top score from database
    $http({
        method: 'POST',
        url: baseURL + 'player/topscore/',
        data: {},
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    })
    .then(function(response) {
        // success
        $scope.globaltopscore =  response.data.topscore;

        // jScrollPane init
        var settings = {
            showArrows : false,
            verticalDragMinHeight : 50,
            verticalDragMaxHeight : 50,
            autoReinitialise: true
        };
        var pane = $('.scroll-pane')
        pane.jScrollPane(settings);
        var api = pane.data('jsp');
        api.reinitialise();

        var throttleTimeout;
        $(window).bind(
            'resize',
            function()
            {
                if (!throttleTimeout) {
                    throttleTimeout = setTimeout(
                        function()
                        {
                            api.reinitialise();
                            throttleTimeout = null;
                        },
                        50
                    );
                }
            }
        );

    });

    // sort players into descending time order
    $scope.orderByTime = function(player){
        return parseInt(player.time);
    };
    
    // filter players by region/city
    $scope.regionMatch = function(xregion) {
        return function(player) {
            return (player.region == xregion && player.ts == 'region');
        };
    };       

    // filter players by region/city
    $scope.globalMatch = function() {
        return function(player) {
            return player.ts == 'global';
        };
    };       

    // find top scoring player and apply CSS class 
    $scope.isTopScorer = function (player) {
        return {
          "topScorer" : player.time == $scope.globaltopscore
        };
    };
}]);

app.controller('gamePanel', [ '$rootScope', '$scope', '$state', '$stateParams', '$interval', '$cookies', 'gameState', '$http', '$translate',  function( $rootScope, $scope, $state, $stateParams, $interval, $cookies, gameState, $http, $translate) {
	// set up translations for text content
    var playerid,
        highscore,
        hasRequestedTestDrive,
        fail_message_count = 49,
        fail_message_choice,
        fail_pad;


    $scope.gameObject = {}; // parent scope object, shares timer across child views of game
    $scope.playerLanguage = $translate.use();

    $rootScope.$on('$translateChangeSuccess', function () {
        $scope.playerLanguage = $translate.use();
    });

    // fetch world top score from database
    // player/topscore/
    $http({
        method: 'POST',
        url: baseURL + 'player/topscore/',
        data: {},
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    })
    .then(function(response) {
        // success
        $scope.topscore = formatTime(parseInt(response.data.topscore, 10));
        gameState.addState("topscore", parseInt(response.data.topscore, 10));
    });

    // set up animation for 'worthy' overlay
    $scope.gameObject.thisFadeAnimation = new FadeAnimation({
        "images" : $('#worthy .animationImage'),
        "interval": 1500,
        "speed" : 5  
    });

    function startTimer(){
        var today = new Date();
            startTime = today.getTime();

        $scope.gameObject.vTime = 0;
        $scope.gameObject.validationTimerPromise = $interval(function(){
            $scope.gameObject.vTime++;
        }, 1000, false);

        $scope.gameObject.timerPromise = $interval(function(){
            var now = new Date(),
                nowMillis = now.getTime(),
                duration = nowMillis - startTime,
                tempDisplay;

            $scope.gameObject.gameTimer = duration;
            $scope.gameObject.gameTimerDisplay = formatTime(duration); // change scope property
        }, 50, false);

    }
    function stopTimer(){
        // stop the timer
        $interval.cancel($scope.gameObject.timerPromise);
        $interval.cancel($scope.gameObject.validationTimerPromise);
    }

    function resetGameState(){
        $rootScope.$emit('resetGame');

        $scope.gameObject.gameTimerDisplay = formatTime(0);

        $scope.gameObject.showMarker = true;
        $scope.gameObject.showTokens = true;

        $scope.gameObject.thisFadeAnimation.stopInterval();
        $scope.gameObject.showWorthyOverlay = false;
        $scope.gameObject.showPorscheOverlay = false;
        $scope.gameObject.showSubmitLeaderboardButton = true;
        $scope.gameObject.showTestDRiveButton = true;

        playerid = (typeof $cookies.playerid !== 'undefined' && parseInt($cookies.playerid, 10) > 0 ) ? parseInt($cookies.playerid, 10) : -1;
        highscore = (typeof $cookies.highscore !== 'undefined') ? parseInt($cookies.highscore, 10) : 0;
        hasRequestedTestDrive = (typeof $cookies.testdrive !== 'undefined') ? parseInt($cookies.testdrive, 10) : 0; // 1 or 0

        // console.log("playerid : %s highscore : %s hasRequestedTestDrive : %s", playerid, highscore, hasRequestedTestDrive);

        $scope.gameObject.playerid = playerid;
        $scope.gameObject.highscore = highscore;
        $scope.gameObject.testdrive = hasRequestedTestDrive;

        gameState.addState("playerid", playerid);
        gameState.addState("highscore", highscore);
        gameState.addState("testdrive", hasRequestedTestDrive);
    }

    $scope.$on('PLAYER_START', function(evt, msg) {
        $scope.gameObject.showMarker = false;
        startTimer();
    });

    $scope.$on('PLAYER_DIED', function(evt, msg) {
        $scope.playerDies();
    });

    // reset game when player clicks 'play again' button in win/lose game overlays
    $scope.resetGame = function(e){
        thisAgilityGame.resetObjects();
        resetGameState();
    };

    // handle end of game, select which overlay to display
    $scope.playerDies = function(){
        var nextState,
            playerid,
            highscore;

        // stop the timer
        $interval.cancel($scope.gameObject.timerPromise);
        $interval.cancel($scope.gameObject.validationTimerPromise);

        console.log("gametimer : %s validation timer: %s", $scope.gameObject.gameTimer, $scope.gameObject.vTime);

        if ( (  ( $scope.gameObject.vTime / ($scope.gameObject.gameTimer/1000)) * 100 ) < 75) {
            gameState.addState("pflag", 2);
        } else {
            gameState.addState("pflag", 0);
        }

        $scope.gameObject.showTokens = false;
        gameState.addState("time", $scope.gameObject.gameTimer);
        
        // if the player lasts longer than the time threshold
        if ( $scope.gameObject.gameTimer > timeThreshold ) {
            // store players time

            // check to see if the player has exceed their previous high score
            if ($scope.gameObject.gameTimer > $scope.gameObject.highscore ) {
                // yes, update highscore cookie and store player score to send to leaderboard form
                $scope.gameObject.showSubmitLeaderboardButton = true;
                // $cookies.highscore = $scope.gameObject.gameTimer; don't store the players highscore umless they have posted to leaderboard
                gameState.addState("highscore", $scope.gameObject.gameTimer);
            } else {
                $scope.gameObject.showSubmitLeaderboardButton = false;
            }

            // show worthy overlay and start animation
            $scope.gameObject.showWorthyOverlay = true;
            $scope.gameObject.showPorscheOverlay = false;

            //console.log('time = ' + $scope.gameObject.gameTimer);
            $scope.formattedTime = formatTime($scope.gameObject.gameTimer);

            $scope.worthy_header = 'WORTHY11';
            $scope.gameObject.thisFadeAnimation.startInterval();
        } else {
            // seelect random fail message
            fail_message_choice = Math.floor (Math.random() * fail_message_count ) + 1;
            fail_pad = ( fail_message_choice < 10) ? '0' : '';
            $scope.fail_message = 'FAIL_' + fail_pad + fail_message_choice;

            $scope.gameObject.showWorthyOverlay = false;
            $scope.gameObject.showPorscheOverlay = true;

            // going to be multiple messages in this overlay, maybe 20 or 30
        }
    }

    resetGameState();
}]);

app.controller('gameForm', ['$state', '$translate', '$rootScope', '$scope', '$stateParams', '$http', '$cookies', 'gameState', function($state, $translate, $rootScope, $scope, $stateParams, $http, $cookies, gameState) {
    $scope.user = {};
    $scope.config = {};

    var thisGameState = gameState.getState();

    $scope.formattedGlobalScore = formatTime(thisGameState.topscore);
    $scope.formattedTime = formatTime(thisGameState.time);
    $scope.user.playerid = thisGameState.playerid;
    $scope.user.time = thisGameState.time;
    $scope.user.playerLanguage = $translate.use();
    $scope.user.pflag = thisGameState.pflag;
    $scope.user.sector = {"id":""};
    $scope.user.region = {"id":""};

    $scope.regionIsInvalid = false;
    $scope.sectorIsInvalid = false;

    // can't instantaite jScrollPane when the view loads as it breaks the angular ng-repeat DOM insert
    
    // update translations for city and sector dropdowns
    $scope.setLanguage = function() {
        $translate(['region_label', 'sector_label', 'uk', 'us', 'belgium', 'australia', 'spain', 'middleeast', 'canada', 'japan', 'china', 'france', 'germany', 'italy', 'switzerland', 'holland', 'luxembourg', 'hongkong', 'region_other', 'finance', 'technology', 'law', 'automotive', 'media', 'real_estate', 'legal', 'pharma', 'construction', 'entrepreneur', 'sector_other']).then(function(translations){

            $scope.config.regionPlaceholder = {"html" : translations.region_label, "tr" : 'region_label'};
            $scope.config.sectorPlaceholder = {"html" : translations.sector_label, "tr" : 'sector_label'};

            $scope.config.region = [
                {"name" : translations.uk, "id" : "uk", "tr" : "uk"},
                {"name" : translations.us, "id" : "us", "tr" : "us"},
                {"name" : translations.belgium, "id" : "belgium", "tr" : "belgium"},
                {"name" : translations.australia, "id" : "australia", "tr" : "australia"},
                {"name" : translations.spain, "id" : "spain", "tr" : "spain"},
                {"name" : translations.middleeast, "id" : "middleeast", "tr" : "middleeast"},
                {"name" : translations.canada, "id" : "canada", "tr" : "canada"},
                {"name" : translations.japan, "id" : "japan", "tr" : "japan"},
                {"name" : translations.china, "id" : "china", "tr" : "china"},
                {"name" : translations.france, "id" : "france", "tr" : "france"},
                {"name" : translations.germany, "id" : "germany", "tr" : "germany"},
                {"name" : translations.italy, "id" : "italy", "tr" : "italy"},
                {"name" : translations.switzerland, "id" : "switzerland", "tr" : "switzerland"},
                {"name" : translations.holland, "id" : "holland", "tr" : "holland"},
                {"name" : translations.luxembourg, "id" : "luxembourg", "tr" : "luxembourg"},
                {"name" : translations.hongkong, "id" : "hongkong", "tr" : "hongkong"}, 
                {"name" : translations.region_other, "id" : "region_other", "tr" : "region_other"}
            ];

            $scope.config.sector = [
                {"name" : translations.finance, "id" : "finance", "tr" : "finance"},
                {"name" : translations.technology, "id" : "technology", "tr" : "technology"},
                {"name" : translations.law, "id" : "law", "tr" : "law"},
                {"name" : translations.automotive, "id" : "automotive", "tr" : "automotive"},
                {"name" : translations.media, "id" : "media", "tr" : "media"},
                {"name" : translations.real_estate, "id" : "real_estate", "tr" : "real_estate"},
                {"name" : translations.legal, "id" : "legal", "tr" : "legal"},
                {"name" : translations.pharma, "id" : "pharma", "tr" : "pharma"},
                {"name" : translations.construction, "id" : "construction", "tr" : "construction"},
                {"name" : translations.entrepreneur, "id" : "entrepreneur", "tr" : "entrepreneur"},
                {"name" : translations.sector_other, "id" : "sector_other", "tr" : "sector_other"}
            ];
            $scope.user.playerLanguage = $translate.use();
        });
    };
    
    // update view if the language changes after the view is shown
    $rootScope.$on('$translateChangeSuccess', function () {
        $scope.setLanguage();
    });
    
    // apply current langauge to view when it loads
    $scope.setLanguage();
    
    $scope.playAgain = function () {
        $state.go('game');
    };

    // handle form submission
    $scope.submitTheForm = function () {
        // manually set form state
        $scope.playerform.$submitted = true;
        $scope.playerform.$touched = true;

        $scope.regionIsInvalid = ($scope.user.region.id === '' || typeof $scope.user.region.id == 'undefined');
        $scope.sectorIsInvalid = ($scope.user.sector.id === '' || typeof $scope.user.sector.id == 'undefined');

        if ($scope.playerform.$valid) {
            // form field validate
            // submit form data to server via AJAX call

            var formSerial = $('#playerform').serialize();
            formSerial = formSerial + '&time=' + $scope.user.time;
            formSerial = formSerial + '&region=' + encodeURIComponent($scope.user.region.id);
            formSerial = formSerial + '&sector=' + encodeURIComponent($scope.user.sector.id);
            formSerial = formSerial + '&language=' + encodeURIComponent($scope.user.playerLanguage);
            formSerial = formSerial + '&pflag=' + $scope.user.pflag;

            $.jCryption.authenticate(controllBox, "backend/jcryption.php?getPublicKey=true", "backend/jcryption.php?handshake=true", function(AESKey) {
                var encryptedString = $.jCryption.encrypt(formSerial, controllBox);

                $.ajax({
                    url: "backend/helpers/save.php",
                    dataType: "json",
                    type: "POST",
                    data: {
                        jCryption: encryptedString
                    },
                    success: function(response) {
                        $cookies.highscore = $scope.user.time;
                        // player data saved successfully, go to 'share' state
                        $state.go('share');
                    }
                });
            }, function() {
               confirm("Are you sure you want to submit this form unencrypted?", function() {
                      $(base.$el).submit();
                  });
            });

        } else {
        }
    }
}]);

app.controller('register', ['$state', '$translate', '$rootScope', '$scope','$stateParams', '$http', '$cookies', 'gameState', function($state, $translate, $rootScope, $scope, $stateParams, $http, $cookies, gameState) {
    $scope.user = {};
    $scope.config = {};

    $scope.user.emailoptin = 1;
    $scope.user.phoneoptin = 1;

    var thisGameState = gameState.getState();

	$scope.setLanguage = function() {
        $translate(["COUNTRY_LABEL","title_mr", "title_miss", "title_ms", "title_mrs", "title_dr", "title_prof", "country_au", "country_at", "country_bh", "country_be", "country_br", "country_ca", "country_cn", "country_cy", "country_cz", "country_dk", "country_ee", "country_fr", "country_de", "country_gr", "country_hu", "country_in", "country_id", "country_it", "country_jp", "country_kw", "country_lb", "country_lu", "country_my", "country_mx", "country_mc", "country_nl", "country_nz", "country_om", "country_ph", "country_pl", "country_pt", "country_pr", "country_qa", "country_ro", "country_ru", "country_sa", "country_sg", "country_za", "country_kr", "country_es", "country_se", "country_ch", "country_tw", "country_th", "country_ae", "country_gb", "country_us" ])
        .then(function(translations){

            $scope.config.country = [
                {"id" : "au", "tr" : "country_au", "name" : translations.country_au},
                {"id" : "at", "tr" : "country_at", "name" : translations.country_at},
                {"id" : "bh", "tr" : "country_bh", "name" : translations.country_bh},
                {"id" : "be", "tr" : "country_be", "name" : translations.country_be},
                {"id" : "br", "tr" : "country_br", "name" : translations.country_br},
                {"id" : "ca", "tr" : "country_ca", "name" : translations.country_ca},
                {"id" : "cn", "tr" : "country_cn", "name" : translations.country_cn},
                {"id" : "cy", "tr" : "country_cy", "name" : translations.country_cy},
                {"id" : "cz", "tr" : "country_cz", "name" : translations.country_cz},
                {"id" : "dk", "tr" : "country_dk", "name" : translations.country_dk},
                {"id" : "ee", "tr" : "country_ee", "name" : translations.country_ee},
                {"id" : "fr", "tr" : "country_fr", "name" : translations.country_fr},
                {"id" : "de", "tr" : "country_de", "name" : translations.country_de},
                {"id" : "gr", "tr" : "country_gr", "name" : translations.country_gr},
                {"id" : "hu", "tr" : "country_hu", "name" : translations.country_hu},
                {"id" : "in", "tr" : "country_in", "name" : translations.country_in},
                {"id" : "id", "tr" : "country_id", "name" : translations.country_id},
                {"id" : "it", "tr" : "country_it", "name" : translations.country_it},
                {"id" : "jp", "tr" : "country_jp", "name" : translations.country_jp},
                {"id" : "kw", "tr" : "country_kw", "name" : translations.country_kw},
                {"id" : "lb", "tr" : "country_lb", "name" : translations.country_lb},
                {"id" : "lu", "tr" : "country_lu", "name" : translations.country_lu},
                {"id" : "my", "tr" : "country_my", "name" : translations.country_my},
                {"id" : "mx", "tr" : "country_mx", "name" : translations.country_mx},
                {"id" : "mc", "tr" : "country_mc", "name" : translations.country_mc},
                {"id" : "nl", "tr" : "country_nl", "name" : translations.country_nl},
                {"id" : "nz", "tr" : "country_nz", "name" : translations.country_nz},
                {"id" : "om", "tr" : "country_om", "name" : translations.country_om},
                {"id" : "ph", "tr" : "country_ph", "name" : translations.country_ph},
                {"id" : "pl", "tr" : "country_pl", "name" : translations.country_pl},
                {"id" : "pt", "tr" : "country_pt", "name" : translations.country_pt},
                {"id" : "pr", "tr" : "country_pr", "name" : translations.country_pr},
                {"id" : "qa", "tr" : "country_qa", "name" : translations.country_qa},
                {"id" : "ro", "tr" : "country_ro", "name" : translations.country_ro},
                {"id" : "ru", "tr" : "country_ru", "name" : translations.country_ru},
                {"id" : "sa", "tr" : "country_sa", "name" : translations.country_sa},
                {"id" : "sg", "tr" : "country_sg", "name" : translations.country_sg},
                {"id" : "za", "tr" : "country_za", "name" : translations.country_za},
                {"id" : "kr", "tr" : "country_kr", "name" : translations.country_kr},
                {"id" : "es", "tr" : "country_es", "name" : translations.country_es},
                {"id" : "se", "tr" : "country_se", "name" : translations.country_se},
                {"id" : "ch", "tr" : "country_ch", "name" : translations.country_ch},
                {"id" : "tw", "tr" : "country_tw", "name" : translations.country_tw},
                {"id" : "th", "tr" : "country_th", "name" : translations.country_th},
                {"id" : "ae", "tr" : "country_ae", "name" : translations.country_ae},
                {"id" : "gb", "tr" : "country_gb", "name" : translations.country_gb},
                {"id" : "us", "tr" : "country_us", "name" : translations.country_us}
            ];

            // check to see if current language is french
            if ($translate.use() === 'fr' ) {
                // if so build smaller set of options for 'title' dropdown
                $scope.config.title = [
                    {"id" : "mr", "tr" : "title_mr", "name" : translations.title_mr},
                    {"id" : "miss", "tr" : "title_miss", "name" : translations.title_miss},
                    {"id" : "mrs", "tr" : "title_mrs", "name" : translations.title_mrs},
               ];
            } else {
                $scope.config.title = [
                    {"id" : "mr", "tr" : "title_mr", "name" : translations.title_mr},
                    {"id" : "miss", "tr" : "title_miss", "name" : translations.title_miss},
                    {"id" : "ms", "tr" : "title_ms", "name" : translations.title_ms},
                    {"id" : "mrs", "tr" : "title_mrs", "name" : translations.title_mrs},
                    {"id" : "dr", "tr" : "title_dr", "name" : translations.title_dr},
                    {"id" : "prof", "tr" : "title_prof", "name" : translations.title_prof}
               ];
            }
            $scope.user.playerLanguage = $translate.use();
         });
    };
    
    // update view if the language changes after the view is shown
    $rootScope.$on('$translateChangeSuccess', function () {
        $scope.setLanguage();
    });
    
    // apply current langauge to view when it loads
    $scope.setLanguage();

   // handle form submission
    $scope.submitTheForm = function () {
        
        // manually set form state
        $scope.playerform.$submitted = true;
        $scope.playerform.$touched = true;

        $scope.countryIsInvalid = ($scope.user.country.id === '' || typeof $scope.user.country.id == 'undefined');
        $scope.titleIsInvalid = ($scope.user.title.id === '' || typeof $scope.user.title.id == 'undefined');

        if ($scope.playerform.$valid) {
            // form field validate
            // submit form data to server via AJAX call

            $http({
                method: 'POST',
                url: baseURL + 'register/',
                data: {
                    "title" : $scope.user.title.id, // custom select object return object not string, use id property
                    "forename" : $scope.user.forename,
                    "surname" : $scope.user.surname,
                    "phone" : $scope.user.phone,
                    "email" : $scope.user.email,
                    "country" : $scope.user.country.id, // custom select object return object not string, use id property
                    "city" : $scope.user.city,
                    "emailoptin" : $scope.user.emailoptin,
                    "phoneoptin" : $scope.user.phoneoptin,
                    "utmsource" : thisGameState.source
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
            })
            
            .then(function(response) {
                // success
                $cookies.testdrive = 1;
                gameState.addState("testdrive", 1);

                // player data saved successfully, go to 'thanks' state
                $state.go('thanks');
            }, 
            function(response) { // optional
                // failed
            });

        } else {
        }
    }
}]);

app.controller('gameShare', ['$rootScope', '$scope', '$translate', 'gameState', function($rootScope, $scope, $translate, gameState) {
    var thisGameState = gameState.getState(),
        shareMessage; 

    $scope.message = {};
    $scope.time = formatTime(thisGameState.time);

    function setShareMessage() {
        $scope.message.language = $translate.use();
        $translate(["share_1", "share_2" ]).then(function(translations){
            $scope.message.share1 = translations.share_1; 
            $scope.message.share2 = translations.share_2; 
            shareMessage = $scope.message.share1 + String($scope.time.seconds) + '.' + String($scope.time.milliseconds) + $scope.message.share2;
            // console.log("language: '%s' message : '%s'", $scope.message.language, shareMessage);
        });
    }
    
    $rootScope.$on('$translateChangeSuccess', function () {
        setShareMessage();
    });

    var shareLink = 'http://lotuscars.com/itsnotforyou';
   $('a.fb').click(function(e){
    
        e.preventDefault();
        var pic,
            name,
            caption,
            description;

        //pic = 'http://www.lotuscars.com/sites/all/themes/lotus_obb/img/lotus-logo.png';
        pic = 'http://www.lotuscars.com/itsnotforyou/img/game.png';
        name = 'LOTUS Evora E400 #ITSNOTFORYOU';
        caption = 'E400 CAPTION';
        description = 'LOTUS Evora E400 DESCRIPTION';            

        FB.ui({
          method: 'feed',
          link: shareLink,
          picture: pic,
          name: name,
          caption: '',//caption,
          description: shareMessage
          //to: friendid
        }, function(response){});    

    });    

    $('a.tweet').click(function(e){
      e.preventDefault();
      //We get the URL of the link
      //var loc = $(this).attr('href');
      var loc = shareLink;
      //We get the title of the link
      var title  = escape($(this).attr('title'));
      //We trigger a new window with the Twitter dialog, in the middle of the page
      window.open('http://twitter.com/share?url=' + loc + '&text=' + shareMessage + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    });

    setShareMessage();
}]);

app.controller('thanks', ['$translate', '$scope',  function($translate, $scope) {
}]);

/* TEST CODE - resets cookie values */
app.controller('reset', ['$scope', '$cookies', function($scope, $cookies) {
    $cookies.language = 'en';
    $cookies.playerid = -1;
    $cookies.highscore = 0;
    $cookies.testdrive = 0;
}]);
/* TEST */

/* agility game controller  */

/* controller for game 'widget' html in templetes/game_play.html
runs *EVERY* time the user goes to the 'agility test' state
the DOM is rebuilt and the game behaviour needs to be rebound to the DOM
*/
app.controller('GameCtrl', ['$rootScope', '$scope', '$interval', '$timeout', function ($rootScope, $scope, $interval, $timeout) { 

    // generate random directions
    var opt = [-1,1],
        optset = [],
        optcount = 0,
        optlimit = 8,
        rand = Math.floor(Math.random() * 2);

    while( optcount < optlimit ) {
        optset.push(opt[Math.floor(Math.random() * 2)]);
        optcount++;
    }

    // cache jquery objects for player and enemy objects in playing field, plus left/top coords for same
    thisAgilityGame = new AgilityGame({   
        "angularScope" : $scope,
        "angularTimeout" : $timeout,
        "gameTestMode" : 0, // 0 - normal, 1 = collision detection disabled
        "touchEnabled" : $('html').hasClass('touch'),
        "playfield" : $('.arena'),
        "playerBox" : $('#box'),
        "allObjects" : $('.object'),
        "enemies" : [
            $('#enemy0'),
            $('#enemy1'),
            $('#enemy2'),
            $('#enemy3')
        ],
        "enemyDirection" : [
            {"x" : optset[0], "y" : optset[1]},
            {"x" : optset[2], "y" : optset[3]},
            {"x" : optset[4], "y" : optset[5]},
            {"x" : optset[6], "y" : optset[7]}
        ],
        "enemySpeed" : [
            {"x" : -5, "y" : 6},
            {"x" : -6, "y" : -10},
            {"x" : 7, "y" : -7},
            {"x" : 8, "y" : 5}
        ]
    });
        
}]);

