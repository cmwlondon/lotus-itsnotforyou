/* utility functions */
var pageHeaderNode,
    languageSelectorIsOpen = false;;

// $(document).ready(function(){});

/*
formatTime(duration)
	duration: time in milliseconds
returns:
{
	"seconds" : seconds (2-digit zero-padded string),
	"milliseconds" : milliseconds (2-digit zero-padded string)
}
*/

function logEvent(e){ window.console && console.log(e.type, e); }

function formatTime(duration){
    var seconds,
        milliseconds,
        timeDisplay = {};
    
    seconds = Math.floor(duration / 1000);
    milliseconds = Math.floor((duration - (seconds * 1000)) / 10);

    timeDisplay.seconds = (seconds < 10) ? '0' : '';
    timeDisplay.seconds = timeDisplay.seconds + seconds;
    timeDisplay.milliseconds = (milliseconds < 10) ? '0' : '';
    timeDisplay.milliseconds = timeDisplay.milliseconds + milliseconds;
    return timeDisplay;
}

/* FadeAnimation */
/*
used to animate background image in 'worthy' overlay (game controller / game.html template)

    $scope.gameObject.thisFadeAnimation = new FadeAnimation({
        "images" : [$('#pic1'), $('#pic2'), $('#pic3')],
        "interval": 1500,
        "speed" : 5  
    });
*/

(function () {
    "use strict";
    
    /*
    RequestAnimationFrame polyfill for IE browsers
    */
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                   || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
 
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };


    /*
    ----------------------------------------------------------------------------------------------------------------
    FadeAnimation
    */

    function FadeAnimation(parameters) {
        this.parameters = parameters;

        this.interval = parameters.interval; // interval between animations in milliseconds
        this.speed = parameters.speed; // speed of animation higher is faster
        this.images = parameters.images; // jQuery collection of images

        this.imageCount = this.images.length;
        this.imageIndex = 0;
        this.intervalKey = null;
        this.animationKey = null;
        this.intervalRunning = false;
    
        this.init();
    }
    
    FadeAnimation.prototype = {
        "constructor" : FadeAnimation,
        "template" : function () {var that = this; },
        
        "init" : function () {
            var that = this;
    
            debug({
                "message" : "FadeAnimation.init",
                "values" : []
            });
        },
        "startInterval" : function () {
            var that = this;

            this.intervalRunning = true;

            this.intervalKey = window.setInterval(function(){
                that.startAnimation();
            }, this.interval);
        },
        "stopInterval" : function () {
            if (this.intervalRunning) {
                window.clearInterval(this.intervalKey);
                this.intervalRunning = false;
            }
        },
        "startAnimation" : function () {
            this.animationProgress = 100; // opacity
            this.cycleAnimation();
        },
        "cycleAnimation" : function () {
            var that = this,
                ti = this.imageIndex;

            if (this.animationProgress > 0) {
                this.animationProgress = this.animationProgress - this.speed;
                this.doAnimation();
                this.animationKey = window.requestAnimationFrame(function(){
                    that.cycleAnimation();
                });
            } else {
                this.stopAnimation(ti);
            }
        },
        "stopAnimation" : function (ti) {
            window.cancelAnimationFrame(this.animationKey) ;

            // end of one animation sequence, prepare for next
            this.images.eq(ti).css({"z-index": 0, "opacity" : 1});
            ti = this.nextIndex(ti);
            this.images.eq(ti).css({"z-index": 2});
            ti = this.nextIndex(ti);
            this.images.eq(ti).css({"z-index": 1});
            this.imageIndex = this.nextIndex(this.imageIndex);
        },
        "doAnimation" : function () {
            var that = this;

            this.images.eq(this.imageIndex).css({"opacity" : (this.animationProgress / 100)});
        },
        "nextIndex" : function (index) {
            index++;
            if (index === this.imageCount)
                index = 0;
            return index;
        },
        "previousIndex" : function (index) {
            index--;
            if (index === -1)
                index = this.imageCount - 1;
            return index;
        }
    };
    
    // objects currently sit within the anonymous function scope only, add them to the window object scope
    window.FadeAnimation = FadeAnimation;

    /*
    ----------------------------------------------------------------------------------------------------------------
    AgilityGame
    */

    function AgilityGame(parameters) {
        this.parameters = parameters;

        // import angular scoe and timeout objects into game
        this.angularScope = parameters.angularScope;
        this.angularTimeout = parameters.angularTimeout;

        this.touchEnabled = parameters.touchEnabled;
        this.playfield = parameters.playfield;
        this.playerBox = parameters.playerBox;
        this.allObjects = parameters.allObjects;
        this.enemies = parameters.enemies;
        this.enemyIndex = 0;
        this.enemyCount = this.enemies.length;
        this.enemyDirection = parameters.enemyDirection;
        this.enemySpeed = parameters.enemySpeed;
        this.gameTestMode = parameters.gameTestMode;

        //console.log("test mode: %s", parameters.gameTestMode);
        this.positions = {};
        this.sizes = {};
        this.enemyTimer = null;
        this.gametime = 0;
        this.moving = false;
        this.started = false;
        this.startTime = null;
        this.contact = false;

        this.playerStart = {
            "left" : 0,
            "top" : 0,
        };
        this.init();
    }
    
    AgilityGame.prototype = {
        "constructor" : AgilityGame,
        "template" : function () {var that = this; },
        
        "init" : function () {
            var that = this,
            enemyIndex;

            this.positions = {
                // "playfield" : playfield.position(),
                "playfield" : this.playfield.offset(),
                "player" : {"left" : 155, "top" : 155},
                "enemy" : [{"left" : 230, "top" : 40}, {"left" : 200, "top" : 280}, {"left" : 50, "top" : 240}, {"left" : 50, "top" : 50}]
            };
            this.sizes = {
                // "playfield" : {"width" : parseInt(this.playfield.css('width'), 10), "height" : parseInt(this.playfield.css('height'), 10)},
                "playfield" : {"width" : 320, "height" : 320},
                "player" : {"width" : 40, "height" : 40},
                "enemy" : []
            };

            enemyIndex = 0;
            while (enemyIndex < this.enemyCount) {
                /*
                this.positions.enemy.push({
                    "left" : parseInt(this.enemies[enemyIndex].css('left'), 10),
                    "top" : parseInt(this.enemies[enemyIndex].css('top'), 10)
                });
                */
                this.sizes.enemy.push({
                    "width" : parseInt(this.enemies[enemyIndex].css('width'), 10),
                    "height" : parseInt(this.enemies[enemyIndex].css('height'), 10)
                });
                enemyIndex = enemyIndex + 1;
            }

            $(window).on('resize', function(){
                that.positions.playfield = that.playfield.offset();
            });

            this.bindEvents();
        },
        "bindEvents" : function () {
            var that = this;

           // automoatically starts the game at beginning of drag, without angular interaction
            if (this.touchEnabled) {
 
                this.playerBox
                .unbind('movestart')
                .unbind('move')
                .unbind('moveend')
                .bind('movestart', function(e){
                    // Only listen to one finger
                    if (e.targetTouches && e.targetTouches.length > 1) {
                        e.preventDefault();
                        return;
                    }

                    that.start = {
                      x: parseInt(that.playerBox.css('left')),
                      y: parseInt(that.playerBox.css('top'))
                    };
                    if (!that.started) {
                        that.playerBox.removeClass('pulse');
                        that.angularScope.$emit('PLAYER_START', true);
                        that.started = true;
                        that.moving = true;
                        // emit signal to angular controller to start timer
                        that.moveAllEnemies(); // start enemy movement
                    }

                })
                .bind('move', function(e){
                    var newPos = {
                        "left" : that.start.x + e.distX,
                        "top" : that.start.y + e.distY
                    };

                    that.playerBox.css({"left" : newPos.left + "px", "top" : newPos.top + "px"});
                    that.positions.player = {"left" : newPos.left, "top" : newPos.top};

                    that.checkInBounds(); // check to see if player has left the playfield
                });
            } else {
                $('#box').on('mousedown',function(e){

                    // captures mousdown
                    that.msx = e.pageX;
                    that.msy = e.pageY;
                    that.playerStart.left = that.positions.player.left;
                    that.playerStart.top = that.positions.player.top;

                    that.doStart();
                });
            }
        },
        "doStart" : function () {
            var that = this,
                nTime;

            this.positions.playfield = this.playfield.offset();
            if (!this.started) {
                this.playerBox.removeClass('pulse');
                this.angularScope.$emit('PLAYER_START', true);

                this.secondaryTimerIndex = 0; 
                this.started = true;
                this.moving = true;
        
                // update playfield position for IE offset bug
                that.positions.playfield = that.playfield.offset();

                // detect system time when player starts moving mouse
                /*
                nTime = new Date();
                this.secondaryTimer.push({
                    "start" : nTime.getTime()
                });
                console.log(this.secondaryTimer[this.secondaryTimerIndex].start);
                */

                this.playfield.on('mousemove', function(e){
                    that.mdx = e.pageX - that.msx;
                    that.mdy = e.pageY - that.msy;

                    var newPos = {
                        "left" : that.playerStart.left + that.mdx,
                        "top" : that.playerStart.top + that.mdy
                    };

                    that.playerBox.css({"left" : newPos.left + "px", "top" : newPos.top + "px"});
                    that.positions.player = {"left" : newPos.left, "top" : newPos.top};

                    that.checkInBounds(); // check to see if player has left the playfield
                });

                /*
                playfield.on('mouseleave',function(){
                    playfield.off('mousemove');
                });
                */
                that.moveAllEnemies(); // start enemy movement
            } else {
            }
        },
        "doEnd" : function () {
            // hide all enemies and player
            // allObjects.hide();
            // remove mouemove handler
            this.playfield.off('mousemove');

            this.playerBox
            .unbind('movestart')
            .unbind('move')
            .unbind('moveend');

            this.started = false;
            this.moving = false;
            this.angularScope.$emit('PLAYER_DIED', false);
        },
        "checkInBounds" : function () {
            if ( (this.positions.player.left + 40) >  this.sizes.playfield.width || this.positions.player.left < 0 || (this.positions.player.top + 40) >  this.sizes.playfield.height || this.positions.player.top < 0) {
                this.doEnd();
            }

        },
        "moveAllEnemies" : function () {
            var that = this;

            this.gametime = this.gametime + 1

            /*if (this.gametime >= 0 && this.gametime < 100) this.speed = 80;
            else if (this.gametime >= 100 &&  this.gametime < 200) this.speed = 60;
            else if (this.gametime >= 200 &&  this.gametime < 300) this.speed = 40;
            else if (this.gametime >= 300 &&  this.gametime < 400) this.speed = 30;
            else if (this.gametime >= 400 &&  this.gametime < 500) this.speed = 20;
            else this.speed = 10;*/    

            if (this.gametime >= 0 && this.gametime < 100) this.speed = 100;
            else if (this.gametime >= 100 &&  this.gametime < 200) this.speed = 80;
            else if (this.gametime >= 200 &&  this.gametime < 300) this.speed = 60;
            else if (this.gametime >= 300 &&  this.gametime < 400) this.speed = 50;
            else if (this.gametime >= 400 &&  this.gametime < 500) this.speed = 40;
            else this.speed = 30;    


            this.moveEnemy(0, this.enemySpeed[0].x, this.enemySpeed[0].y);
            this.moveEnemy(1, this.enemySpeed[1].x, this.enemySpeed[1].y);
            this.moveEnemy(2, this.enemySpeed[2].x, this.enemySpeed[2].y);
            this.moveEnemy(3, this.enemySpeed[3].x, this.enemySpeed[3].y);

            if (this.moving) {
                this.gamePromise = setTimeout(function(){
                    that.moveAllEnemies();
                }, this.speed);
                /*
                this.angularScope.gamePromise = this.angularTimeout(function(){
                    that.moveAllEnemies();
                }, this.speed);
                */
            } else {
                window.clearTimeout(this.gamePromise);
                // this.angularTimeout.cancel(this.angularScope.gamePromise);
            }

        },
        "moveEnemy" : function (enemyIndex, step_x, step_y) {
            var that = this,
                thisEnemy = this.enemies[enemyIndex],
                thisEnemyPosition = this.positions.enemy[enemyIndex],
                thisEnemySize = this.sizes.enemy[enemyIndex],
                thisEnemyDirection = this.enemyDirection[enemyIndex],
                newLeft = 0,
                newTop = 0;

            if (thisEnemyPosition.left >= (this.sizes.playfield.width - thisEnemySize.width) || thisEnemyPosition.left <= 0) {
                thisEnemyDirection.x = -1 * thisEnemyDirection.x;
            }
            if (thisEnemyPosition.top >= (this.sizes.playfield.height - thisEnemySize.height) || thisEnemyPosition.top <= 0) {
                thisEnemyDirection.y = -1 * thisEnemyDirection.y;
            }

            newLeft = thisEnemyPosition.left + (step_x * thisEnemyDirection.x) + 0;
            newTop = thisEnemyPosition.top + (step_y * thisEnemyDirection.y) + 0;

            thisEnemy.css({"left" : newLeft + "px", "top" : newTop + "px"});

            thisEnemyPosition.left = newLeft;
            thisEnemyPosition.top = newTop;

            this.checkCollision(enemyIndex); // check to see if an enemy and the player collide

            if (this.contact) {
                this.doEnd(); 
            }

        },
        "checkCollision" : function (enemyIndex) {
            var that = this,
                thisEnemy = this.enemies[enemyIndex],
                thisEnemyPosition = this.positions.enemy[enemyIndex],
                thisEnemySize = this.sizes.enemy[enemyIndex],
                diffX,
                diffY;

            diffX = this.positions.player.left - thisEnemyPosition.left - 0; // -0 converts to integer
            diffY = this.positions.player.top - thisEnemyPosition.top - 0;

            if (diffX > (-1 * this.sizes.player.width) && diffX < thisEnemySize.width && diffY > (-1 * this.sizes.player.height) && diffY < thisEnemySize.height) {
                this.contact = true;
            } else {
                this.contact = false;
            }

        },
        "resetObjects" : function () {
            // generate random directions
            var opt = [-1,1],
                optset = [],
                optcount = 0,
                optlimit = 8,
                rand = Math.floor(Math.random() * 2);
            while( optcount < optlimit ) {
                optset.push(opt[Math.floor(Math.random() * 2)]);
                optcount++;
            }

            this.enemyTimer = null;
            this.gametime = 0;
            this.moving = false;
            this.started = false;
            this.startTime = null;
            this.contact = false;

            this.enemyDirection = [
                {"x" : optset[0], "y" : optset[1]},
                {"x" : optset[2], "y" : optset[3]},
                {"x" : optset[4], "y" : optset[5]},
                {"x" : optset[6], "y" : optset[7]}
            ];
            this.enemySpeed = [
                {"x" : -5, "y" : 6},
                {"x" : -6, "y" : -10},
                {"x" : 7, "y" : -7},
                {"x" : 8, "y" : 5}
            ];

            this.positions.player = {"left" : 155, "top" : 155};
            this.positions.enemy = [{"left" : 230, "top" : 40}, {"left" : 200, "top" : 280}, {"left" : 50, "top" : 240}, {"left" : 50, "top" : 50}];

            this.playerBox.css({"left" : "155px", "top" : "155px"});

            this.enemies[0].css({"left" : "230px", "top" : "40px"});
            this.enemies[1].css({"left" : "200px", "top" : "280px"});
            this.enemies[2].css({"left" : "50px", "top" : "240px"});
            this.enemies[3].css({"left" : "50px", "top" : "50px"});

            this.bindEvents();

        }
    };
    
    // objects currently sit within the anonymous function scope only, add them to the window object scope
    window.AgilityGame = AgilityGame;

})();
