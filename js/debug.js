(function () {
	"use strict";
	
	function debug(parameters) {
		/*
		parameters = {
			"message" : "test [1] [2] ...",
			"values" : [ "red", 666, ... ]
		}
		*/

		var debugCheckURL = window.location.href,
			debugURLSwitch = 'debug=1',
			debugEnabled = debugCheckURL.indexOf(debugURLSwitch) !== -1,
			debugRegEx = /\[([0-9])\]/g,
			matchCollection = [],
			matchCollection2 = [],
			matchIndex = 0,
			matchesFound = false,
			updatedMessage = parameters.message,
			result,
			index = 0,
			valueMarker,
			valueIndex,
			replaceRegex,
			count;
		
		if (debugEnabled) {
			do {
				result = debugRegEx.exec(updatedMessage);
				if (result !== null) {
					matchCollection[matchIndex] = result[1]; // returns: exact match 'N'
					matchIndex += 1;
					matchesFound = true;
				}
			} while (result !== null);
            
			if (matchesFound) {
				// remove duplicate entries
				matchCollection2 = matchCollection.filter(function (elem, pos) {
					return matchCollection.indexOf(elem) === pos;
				});
				count = matchCollection2.length;
				while (index < count) {
					valueMarker = "\\[" + matchCollection2[index] + "\\]";
					valueIndex = parseInt(matchCollection2[index], 10) - 1;
					replaceRegex = new RegExp(valueMarker, 'g');
					updatedMessage = updatedMessage.replace(replaceRegex, parameters.values[valueIndex]);
					index += 1;
				}
			}
			console.log(updatedMessage);
		}
	}
	
	// create dummy console.log function for browsers which do not support console.log natively (IE)
	if (!window.console) {
		window.console = {
			"log" : function () {}
		};
	}

	window.debug = debug;
})();